import { browser, by, element } from 'protractor';

export class MainMenu {

    get() {
        return element(by.id('mainMenu'))
    }

    isVisible(): boolean {
        return this.get() == null
    }
}