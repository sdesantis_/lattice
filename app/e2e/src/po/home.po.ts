import { browser, by, element } from 'protractor';

export class HomePO {
  navigateTo(destination) {
    return browser.get(destination);
  }

  getTitle() {
    return browser.getTitle();
  }

  getPageOneTitleText() {
    return element(by.tagName('home-home')).element(by.tagName('ion-title')).element(by.css('.toolbar-title')).getText();
  }
}
