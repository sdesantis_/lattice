import { browser, by, element } from 'protractor';

export class AdminHomePO {

  navigateTo(destination) {
    return browser.get(destination)
  }



  clickUserSearch() {

  }

  getTitle() {
    return browser.getTitle()
  }

  getPageOneTitleText() {
    return element(by.tagName('home-home')).element(by.tagName('ion-title')).element(by.css('.toolbar-title')).getText();
  }
}
