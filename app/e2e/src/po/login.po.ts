import {browser, by, element} from 'protractor'
import {ionInputById} from "../util/ionic-protractor"

export class LoginPO {
    navigateTo(destination) {
        return browser.get(destination)
    }

    login(email: string, password: string) {
        this.typeEmail(email)
        this.typePassword(password)
        this.clickSignIn()
    }

    getTitle() {
        return browser.getTitle()
    }

    getEmailField() {
        // return ionInputById('email')
        return element(by.ionInput('email'))
    }

    getPasswordField() {
        // return ionInputById('password')
        return element(by.ionInput('password'))
    }

    getLoginButton() {
        return element(by.id('login'))
    }

    getRegisterButton() {
        return element(by.id('register'))
    }

    getResetPasswordLink() {
        return element(by.id('resetPassword'))
    }

    typeEmail(keys) {
        console.log('email', this.getEmailField())
        return this.getEmailField().sendKeys(keys)
    }

    typePassword(keys) {
        return this.getPasswordField().sendKeys(keys)
    }

    clickSignIn() {
        return this.getLoginButton().click()
    }
}
