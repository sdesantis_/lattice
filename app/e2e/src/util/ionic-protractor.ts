import {browser, element, by, ElementFinder} from 'protractor'
import {promise} from "selenium-webdriver"

by.addLocator('ionInput', (name, opt_parentElement) => {
    const using = opt_parentElement || document,
        ionInput = using.querySelector('ion-input[name="' + name + '"]')

    if (ionInput) {
        return ionInput.shadowRoot.querySelector('input')
    } else {
        return
    }
})

export function ionInputById(id: string) {
    return element(by.id(id)).shadowRoot.querySelector('input')
}

/**
 *
 * @param screenSelector The value for the selector property in the page's @Component annotation.
 *                       This ensure we only select element in the page we're interested in
 * @returns {any}
 */
export function toolbarTitle(screenSelector): Promise<string> {
    let page = element(by.css(screenSelector))
    return toPromise(page.element(by.css('.toolbar-title')).getAttribute('innerHTML'))
}

export function expectToolbarTitleIs(title: string) {
    expect(element(by.css('.toolbar-title')) // Get the ionic toolbar title element
        .getAttribute('innerHTML')) // Get the text content
        .toContain(title)
}


function toPromise<T>(p: promise.Promise<T>): Promise<T> {
    return new Promise((resolve, reject) => p.then(resolve, reject))
}