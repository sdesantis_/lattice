import {browser} from "protractor"

export function random6digits() {
    return 100000 + Math.floor(Math.random() * 900000)
}


/**
 * [selectWindow Focus the browser to the index window. Implementation by http://stackoverflow.com/questions/21700162/protractor-e2e-testing-error-object-object-object-has-no-method-getwindowha]
 * @param  {[Object]} index [Is the index of the window. E.g., 0=browser, 1=FBpopup]
 * @return {[!webdriver.promise.Promise.<void>]}       [Promise resolved when the index window is focused.]
 */
export async function selectWindow(index) {

    // wait for handels[index] to exists
    browser.driver.wait(async function () {
        let handles = await browser.driver.getAllWindowHandles()
        /**
         * Assume that handles.length >= 1 and index >=0.
         * So when i call selectWindow(index) i return
         * true if handles contains that window.
         */
        if (handles.length > index)
            return true
    })
    // here i know that the requested window exists

    // switch to the window
    let handles = await browser.driver.getAllWindowHandles()
    return browser.driver.switchTo().window(handles[index])
}