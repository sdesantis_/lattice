import {TestBed} from '@angular/core/testing'
import {TranslateService} from "@ngx-translate/core"
import {FormsModule, ReactiveFormsModule} from "@angular/forms"
import {
    IonApp,
    Config,
    IonicModule,
    DomController,
    MenuController,
    NavController,
    Platform, LoadingController, ToastController, ActionSheetController, PopoverController
} from '@ionic/angular'
import {IonicStorageModule} from "@ionic/storage"
import {AppVersion} from "@ionic-native/app-version/ngx"
import {Log} from "ng-log"
import {Camera} from "@ionic-native/camera/ngx"
import {UIUtil} from "../app/pages/UIUtil"
import {LogSubmitter} from "../app/services/log-submitter.service"
import {AppVersionMock, ConfigMock, LogMock, PlatformMock} from "./mocks"
import {TranslatePipeMock, TranslateServiceMock} from "./translate.mock"
// import { ConfigMock, PlatformMock } from '@ionic/mocks';
// import {AppVersionMock, LogMock, TranslatePipeMockModule} from "./mocks"


export class TestUtils {

    public static beforeEachCompiler(components: Array<any>, providers: any[] = []): Promise<{ fixture: any, instance: any }> {
        return TestUtils.configureIonicTestingModule(components, providers)
            .compileComponents().then(() => {
                let fixture: any = TestBed.createComponent(components[0])
                return {
                    fixture: fixture,
                    instance: fixture.debugElement.componentInstance,
                }
            })
    }

    public static configureIonicTestingModule(components: Array<any>, providers: any[]): typeof TestBed {
        return TestBed.configureTestingModule({
            declarations: [
                ...components,
                // pipes etc
                TranslatePipeMock
            ],
            providers: [
                IonApp, Camera, DomController, MenuController, NavController,
                {provide: Platform, useFactory: () => new PlatformMock()},
                {provide: Config, useFactory: () => new ConfigMock()},
                {provide: TranslateService, useInstance: new TranslateServiceMock()},

                {provide: LogSubmitter, useInstance: {}},
                {provide: AppVersion, useClass: AppVersionMock},
                {provide: Log, useClass: LogMock},

                LoadingController, ToastController, ActionSheetController, PopoverController,
                UIUtil,
                ...providers
            ],
            imports: [
                FormsModule,
                IonicModule,
                ReactiveFormsModule,
                IonicStorageModule.forRoot()
            ],
        })
    }

    // http://stackoverflow.com/questions/2705583/how-to-simulate-a-click-with-javascript
    public static eventFire(el: any, etype: string): void {
        if (el.fireEvent) {
            el.fireEvent('on' + etype)
        } else {
            let evObj: any = document.createEvent('Events')
            evObj.initEvent(etype, true, false)
            el.dispatchEvent(evObj)
        }
    }
}
