import {EventEmitter, NgModule, Pipe} from '@angular/core'
import { Observable } from 'rxjs';

/**
 * Needed so that `aot` build is working. But it isn't used throughout our tests and/or app.
 */
@Pipe({name: 'translate'})
export class TranslatePipeMock {
    public transform(value): string {
        return value
    }
}

@NgModule({
    declarations: [TranslatePipeMock]
})
export class TranslatePipeMockModule {}


export class TranslateServiceMock {

  public currentLang: string = 'en';

  public get(value: any): Observable<string> {
    return Observable.create(value);
  }

  public onTranslationChange: EventEmitter<string> = new EventEmitter();
  public onLangChange: EventEmitter<string> = new EventEmitter();
  public onDefaultLangChange: EventEmitter<string> = new EventEmitter();

  /* tslint:disable */
  public use(value: any): void {}
  public set(lang: string): void {}
  public setDefaultLang(lang: string): void {}
 /* tslint:enable */
}
