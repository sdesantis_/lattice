import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {FormsModule} from '@angular/forms'
import {IonicModule} from '@ionic/angular'
import {environment} from "../environment"
import {HttpClient} from "@angular/common/http"
import {AngularFireModule} from "@angular/fire"
import {AngularFireDatabaseModule} from "@angular/fire/database"
import {TranslateLoader, TranslateModule} from "@ngx-translate/core"
import {TranslateHttpLoader} from "@ngx-translate/http-loader"
import {IonicStorageModule} from "@ionic/storage"
import {AngularFireMessagingModule} from "@angular/fire/messaging"
import {AngularFirestoreModule} from "@angular/fire/firestore"

export function HttpLoaderFactory(http: HttpClient) {
    // The prefix needs to a be relative path as in Android the index file is file:///android_asset/www/index.html
    return new TranslateHttpLoader(http, 'assets/i18n/')
}

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        AngularFireModule.initializeApp({
            apiKey: environment.gcpApiKey,
            authDomain: `${environment.gcpProjectId}.firebaseapp.com`,
            databaseURL: `https://${environment.gcpProjectId}.firebaseio.com`,
            projectId: environment.gcpProjectId,
            storageBucket: `${environment.gcpProjectId}.appspot.com`,
            messagingSenderId: environment.gcpProjectNumber
        }),
        AngularFireDatabaseModule,
        AngularFirestoreModule,
        AngularFireMessagingModule,
    ],
    declarations: [],
    entryComponents: [],
})
export class ComponentsModule {
}
