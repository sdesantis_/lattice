import {Injectable} from "@angular/core"
import {ToastController, LoadingController} from "@ionic/angular"
import {Log} from "ng-log"
import {TranslateService} from "@ngx-translate/core"

@Injectable()
export class UIUtil {

    log: Log = new Log(UIUtil.name)

    constructor(private loadingCtrl: LoadingController,
                private toastCtrl: ToastController,
                private translate: TranslateService) {}

    /**
     * Wraps an asynchronous call with the LoadingController overlay
     * @param action the action to execute
     * @param loadingMessage the loading message to display
     * @returns {Promise<T>}
     */
    async loading<T>(action: Promise<T>, loadingMessage?: string): Promise<T> {
        // Delay showing the loading spinner for 0.4s
        // See http://yuiblog.com/blog/2010/11/11/improving-perceived-site-performance-with-spinners/
        // Hopefully delay is re-implemented. See https://github.com/ionic-team/ionic/pull/11583
        let loadingOptions: any/*: Opts*/ = {} // delay: 400
        if (loadingMessage)
            loadingOptions.content = loadingMessage
        //     content: 'Please wait...',
        //         spinner: 'crescent',
        //     duration: 2000
        // }
        let loadingElement  = await this.loadingCtrl.create(loadingOptions)
        await loadingElement.present()
        try {
            let result: T = await action
            loadingElement.dismiss()
            return result
        } catch (e) {
            loadingElement.dismiss()
            throw e
        }
    }


    logErrorAndToast(displayMessage: string, error: any) {
        this.toastCtrl.create({message: displayMessage, duration: 3000}).then(overlay => overlay.present(), error => this.log.warn(error))
        this.log.error(displayMessage, error)
    }

    toast(message: string) {
        this.toastCtrl.create({message: message, duration: 3000}).then(overlay => overlay.present(), error => this.log.warn(error))
    }

    toastTranslate(messageKey: string) {
        this.toastCtrl.create({message: this.translate.instant(messageKey), duration: 3000}).then(overlay => overlay.present(), error => this.log.warn(error))
    }


    imageUrlToBase64(url: string): Promise<string> {
        return new Promise((resolve, reject) => {
            const img = new Image()
            img.crossOrigin = 'anonymous'
            img.onerror = function (error) {
                reject(error)
            }
            img.onload = function () {
                const canvas = document.createElement('canvas')
                canvas.width = img.width
                canvas.height = img.height

                const ctx = canvas.getContext('2d')
                if(ctx === null) {
                    reject('Unable to get canvas 2d context')
                    return
                }
                ctx.drawImage(img, 0, 0)

                resolve(canvas.toDataURL('image/png'))
            }
            img.src = url
        })
    }

    static convertImgIdToBase64(imgId: string): string {
        if (!imgId)
            throw 'convertImgIdToBase64 requires imgId'
        const img = <HTMLImageElement>document.getElementById(imgId)
        if (!img)
            throw 'Could not find element with id ' + imgId
        return UIUtil.convertImgToBase64(img)
    }

    static convertImgToBase64(imgElement:HTMLImageElement): string {
        // imgElem must be on the same server or,
        // the img element must have the crossorigin="anonymous" attribute and the server set the otherwise CORS header
        // otherwise a cross-origin error will be thrown "SECURITY_ERR: DOM Exception 18"
        const canvas = document.createElement('canvas')
        canvas.width = imgElement.clientWidth
        canvas.height = imgElement.clientHeight
        const ctx = canvas.getContext('2d')
        if(ctx === null) throw 'Unable to get canvas 2d context'
        ctx.drawImage(imgElement, 0, 0)
        const dataURL = canvas.toDataURL('image/png')
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, '')
    }

}
