import {Component} from '@angular/core'
import {Router} from "@angular/router"

@Component({
    selector: 'app-page-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {


    constructor(private router: Router) {
    }


    gotoAdmin() {
        this.router.navigateByUrl('/admin/admin-home').catch(console.error)
    }
}
