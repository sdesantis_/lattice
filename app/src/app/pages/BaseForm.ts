import {AbstractControl, FormArray, FormControl, FormGroup} from "@angular/forms"
import {OnInit} from "@angular/core"

export abstract class BaseForm implements OnInit {

    public form: FormGroup
    public serverError: string | null
    public submitted: boolean = false
    events: any[] = [] // list of form changes

    abstract ngOnInit(): void

    /**
     * Sets submitted to true, serverError to null and marks all the form controls as touched, which triggers the validations
     */
    protected setSubmitted() {
        this.serverError = null
        this.submitted = true
        this._markAsTouched(this.form)
    }

    private _markAsTouched(group: FormGroup | FormArray) {
        group.markAsTouched()
        // controls is an object in FormGroup and an Array in FormArray
        // TODO probably missing hasOwnProperty
        for (let i in group.controls) {
            let control = (<any>group.controls)[i]
            if (control instanceof FormControl) {
                // group.controls[i].updateValueAndValidity()
                // group.controls[i].markAsDirty()
                control.markAsTouched()
            } else {
                this._markAsTouched(control)
            }
        }
    }

    /**
     * @param control the control name
     * @returns {any} the control value
     */
    public value(control: string): string {
        return this.form.controls[control].value
    }

    /**
     * Get the value of a control as an int
     * @param {string} control
     * @returns {number}
     */
    public int(control: string): number {
        return parseInt(this.form.controls[control].value)
    }

    /**
     * Get the value of a control as a float
     * @param {string} control
     * @returns {number}
     */
    public float(control: string): number {
        return parseFloat(this.form.controls[control].value)
    }

    /**
     * @param {string} field
     * @returns {boolean} if the field has errors
     */
    public errors(field: string): boolean {
        let control = this.form.controls[field]
        if (!control)
            throw 'Control ' + field + ' does not exist on the form'
        return this.submitted && control.errors !== null && Object.keys(control.errors).length > 0
    }

    /**
     * @param {string} fieldName the field to check
     * @param {string} errorName the particular error to check
     * @returns {boolean} if the field has a particular error and the field has been touched
     */
    public hasError(fieldName: string, errorName: string){
        const control: AbstractControl = this.form.controls[fieldName]
        return control.touched
            && control.errors !== null
            && control.errors[errorName]
    }
}
