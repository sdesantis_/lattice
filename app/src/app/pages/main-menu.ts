import {Injectable} from "@angular/core"

/**
 * Helper for the main menu component
 */
@Injectable()
export class MainMenu {

    showMenu: boolean = false
}