import {Component} from '@angular/core'
import {UserService} from "../../services/user.service"
import {Router} from "@angular/router"
import {Log} from "ng-log"
import {firebase} from '@firebase/app'
import '@firebase/auth'
import {AngularFireAuth} from "@angular/fire/auth"
import {UIUtil} from "../UIUtil"
import {UserInfo} from "firebase"
import {Storage} from "@ionic/storage"

@Component({
    selector: 'app-page-settings',
    templateUrl: 'settings.page.html',
    styleUrls: ['settings.page.scss'],
})
export class SettingsPage {

    log: Log = new Log(SettingsPage.name)

    constructor(private router: Router, private userService: UserService, private afAuth: AngularFireAuth, private uiUtil: UIUtil, private storage: Storage) {

    }


    async logout() {
        try {
            await this.userService.logout()
        } catch (e) {
            this.log.warn('Error logging out', e)
        }
        this.router.navigateByUrl('login')
    }

    async testPush() {
        try {
            await this.userService.testPushNotification()
        } catch (e) {
            this.log.error('Error sending test push', e, e.message)
        }
    }

    hasFacebookProvider(): boolean {
        return !!this.afAuth.auth.currentUser.providerData.find(userInfo => userInfo.providerId === 'facebook.com')
    }

    /**
     * Links the Facebook provider to the users Firebase identity so they can log in using Facebook
     * @returns {Promise<void>}
     */
    private async linkFacebook() {
        this.log.log('Linking Facebook account..')
        const provider = new firebase.auth.FacebookAuthProvider()
        try {
            // TODO link with redirect for Cordova
            const result = await this.afAuth.auth.currentUser.linkWithPopup(provider)
            // Accounts successfully linked.
            var credential = result.credential
            var user = result.user
            this.uiUtil.toast('Facebook account linked')
        } catch (e) {
            if (e.code === 'auth/provider-already-linked')
                this.uiUtil.logErrorAndToast('Facebook account is already linked', e)
            else
                this.uiUtil.logErrorAndToast('Error linking Facebook account', e)
        }
    }

    /**
     * Unlinks the Facebook provider from the users Firebase identity
     * @returns {Promise<void>}
     */
    private async unlinkFacebook() {
        if (this.afAuth.auth.currentUser.providerData.length <= 1) {
            this.uiUtil.toast('Facebook is your only authentication provider. Unable to unlink')
            return
        }
        try {
            const result = await this.afAuth.auth.currentUser.unlink('facebook.com')
            this.uiUtil.toast('Facebook account unlinked')
        } catch (e) {
            this.uiUtil.logErrorAndToast('Error linking Facebook account', e)
        }
    }

    /**
     * Delete the users account and all their data
     */
    async deleteAccount() {
        try {
            await this.userService.deleteAccount()
        } catch (e) {
            this.uiUtil.logErrorAndToast('Error deleting your user data', e)
            return
        }
        try {
            await this.afAuth.auth.currentUser.delete()
        } catch (e) {
            this.uiUtil.logErrorAndToast('Error deleting your user account', e)
            return
        }
        this.storage.clear()
        this.router.navigateByUrl('/')
    }
}
