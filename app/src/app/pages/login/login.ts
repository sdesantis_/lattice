import {Component} from '@angular/core'
import {FormBuilder, Validators} from '@angular/forms'
import {IonApp} from '@ionic/angular'
import {Log} from "ng-log"
import {UIUtil} from "../UIUtil"
import {UserService} from "../../services/user.service"
import {BaseForm} from "../BaseForm"
import {TranslateService} from "@ngx-translate/core"
import {LoginSequence} from "../../services/LoginSequence"
import {FacebookService} from "../../services/facebook.service"
import {environment} from "../../environment"
import {CustomValidators} from "../validators"
import {Subscription} from "rxjs/index"
import {AngularFireAuth} from "@angular/fire/auth"


/**
 *
 */
@Component({
    selector: 'page-login',
    templateUrl: './login.html'
})
export class LoginPage extends BaseForm {

    log: Log = new Log(LoginPage.name)
    appName: string
    authSub: Subscription

    constructor(private fb: FormBuilder,
                private app: IonApp,
                private afAuth: AngularFireAuth,
                private userService: UserService,
                private loginSequence: LoginSequence,
                private uiUtil: UIUtil,
                private facebook: FacebookService,
                private translate: TranslateService) {
        super()
        this.appName = environment.appName
    }

    ngOnInit() {
        this.form = this.fb.group({
            email: ['', Validators.compose([Validators.required, CustomValidators.validateEmail])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
        })
        this.authSub = this.afAuth.authState.subscribe(user => {
            if(user)
                this.loginSequence.next()
        })
    }

    ngDestroy() {
        if(this.authSub)
            this.authSub.unsubscribe()
    }

    gotoResetPassword() {
        throw 'TODO'
    }

    async login() {
        this.log.log('Login')
        this.setSubmitted()
        if (!this.form.valid)
            return

        try {
            let user = await this.uiUtil.loading(this.userService.login(this.value('email'), this.value('password')))
            this.log.log('Logged in', user)
            this.loginSequence.next()
        } catch (error) {
            if (error.code && error.code === 101) { // TODO ParseError.OBJECT_NOT_FOUND = 101;. Message is 'invalid login parameters'
                this.log.log('Invalid email/password entered')
                this.serverError = this.translate.instant('INVALID_EMAIL_PASSWORD')
            } else {
                this.serverError = this.translate.instant('LOGIN_ERROR')
                this.log.error('Login form error', error)
            }
        }
    }

    async register() {
        this.log.log('Register')
        this.setSubmitted()
        if (!this.form.valid)
            return

        try {
            let user = await this.uiUtil.loading(this.userService.register(this.value('email'), this.value('password')))
            this.log.log('Registered', user)
            this.loginSequence.next()
        } catch (error) {
            this.serverError = error.message ? error.message : this.translate.instant('LOGIN_ERROR')
            this.log.log(this.serverError)
        }
    }

    async facebookLogin(): Promise<any> {
        this.log.log('FacebookLogin')
        try {
            // First get the Facebook authenticate token
            let authResponse = await this.facebook.authenticate()
            // Then pass that to the Firebase Facebook login
            await this.uiUtil.loading(this.userService.facebookLogin(authResponse))
            this.loginSequence.next()
        } catch (e) {
            if(e.code === 'auth/account-exists-with-different-credential') {
                this.uiUtil.toast('An account already exist with this email. You will need to login first then link the Facebook account')
                return
            }

            localStorage.clear()
            this.log.error('Facebook login error', e)
            this.uiUtil.toast('Unable to login with Facebook')
        }
    }



}
