import {ComponentFixture, async} from '@angular/core/testing'
import {LoginPage}          from './login'
import {UserService} from "../../services/user.service"
import {LoginSequence} from "../../services/LoginSequence"
import {FacebookService} from "../../services/facebook.service"
import {TestUtils} from "../../../test/test-utils"
import {AngularFireAuth} from "@angular/fire/auth"

let fixture: ComponentFixture<LoginPage> = null
let instance: any = null

// stub UserService for test purposes
let userServiceStub = {
    isLoggedIn: true,
    user: {name: 'Test User'}
}

let loginSequence = {
    next: () => {
    }
}
const facebookServiceMock = {}
const auth = {}

const providers = [
    {provide: UserService, useValue: userServiceStub},
    {provide: LoginSequence, useValue: loginSequence},
    {provide: AngularFireAuth, useValue: auth},
    {provide: FacebookService, useValue: facebookServiceMock},
]

describe('LoginPO', () => {

    beforeEach(async(() => TestUtils.beforeEachCompiler([LoginPage], providers).then(compiled => {
        fixture = compiled.fixture
        instance = compiled.instance
    })))

    it('should create the login page', async(() => {
        expect(instance).toBeTruthy()
    }))
})