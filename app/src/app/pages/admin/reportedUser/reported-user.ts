import {Log} from "ng-log"
import {UIUtil} from "../../UIUtil"
import {Component} from "@angular/core"
import * as _ from 'lodash'
import {TranslateService} from "@ngx-translate/core"
import {IReport, ReportedUserDetails} from "../../../../../../server/src/data-model"
import {AdminService} from "../../../services/admin.service"
import {ActionSheetController, NavParams} from "@ionic/angular"
import {Router} from "@angular/router"

@Component({
    selector: 'reported-user',
    templateUrl: 'reported-user.html'
})
export class ReportedUserPage {

    log: Log = new Log(ReportedUserPage.name)
    report: IReport
    reportDetails: ReportedUserDetails

    constructor(private adminService: AdminService,
                private router: Router,
                private actionSheet: ActionSheetController,
                private translate: TranslateService,
                private uiUtil: UIUtil,) {
        // this.report = navParams.get('report')
        // this.reportDetails = navParams.get('reportDetails')
    }


    async deletePhoto(photoUrl: string) {
        // this.log.log('Deleting photo', photoUrl)
        // try {
        //     await this.uiUtil.loading(this.adminService.deletePhoto(this.report.id, photoUrl))
        //     // On success remove the photo locally
        //     _.remove(<string[]>this.report.profile.photos, (photo: string) => photo === photoUrl)
        //     this.uiUtil.toast('Photo deleted')
        // } catch(e) {
        //     const msg = 'Error deleting photo'
        //     this.log.error(msg, e)
        //     this.uiUtil.toast(msg)
        // }
    }

    /**
     * Display the options action sheet for the reported user profile
     */
    reportOptions() {
        // let actionSheet = this.actionSheet.create({
        //     title: 'Select Report Action',
        //     buttons: [
        //         {
        //             text: 'Close report (No action)',
        //             handler: () => {this.closeReport('none')}
        //         }, {
        //             text: 'Close report (Photo deleted)',
        //             handler: () => {this.closeReport('deleted photo')}
        //         }, {
        //             text: 'Ban user',
        //             handler: () => {this.banUserAction()}
        //         }, {
        //             text: this.translate.instant('CANCEL'),
        //             role: 'cancel'
        //         }
        //     ]
        // })
        // actionSheet.present()
    }

    /**
     * Close the report with info on any action taken
     */
    async closeReport(action: string) {
        // this.log.log('Closing report ' + this.report.id + ' with action ' + action)
        //
        // try {
        //     await this.uiUtil.loading(this.adminService.closeReport(this.report.id, action))
        //     this.nav.pop().catch(this.log.error)
        // } catch(e) {
        //     const msg = 'Error closing report'
        //     this.log.error(msg, e)
        //     this.uiUtil.toast(msg)
        // }
    }

    /**
     * Closes the report, and all other associated with the reported user, and bans the user.
     */
    async banUserAction() {
        // this.log.log('Banning user report id:', this.report.id)
        // try {
        //     await this.uiUtil.loading(this.adminService.banUser(this.report.profile.uid))
        //     this.nav.pop().catch(this.log.error)
        // } catch(e) {
        //     const msg = 'Error banning user'
        //     this.log.error(msg, e)
        //     this.uiUtil.toast(msg)
        // }
    }

}
