import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'
import {AdminGuard} from "../../services/admin-guard.service"
import {AdminHomePage} from "./home/admin-home"
import {PhotoReviewPage} from "./photoReview/photo-review"
import {ReportedUserPage} from "./reportedUser/reported-user"
import {ReportedUsersPage} from "./reportedUsers/reported-users"
import {AdminUserSearchPage} from "./userSearch/user-search"
import {AdminUserPage} from "./user/admin-user"


const adminRoutes: Routes = [
    {path: 'admin-home', component: AdminHomePage},
    {path: 'photo-review', component: PhotoReviewPage},
    {path: 'reported-users', component: ReportedUsersPage},
    {path: 'reported-user/:id', component: ReportedUserPage},
    {path: 'admin-user-search', component: AdminUserSearchPage},
    {path: 'admin-user/:id', component: AdminUserPage},
]
adminRoutes.forEach(route => route.canActivate = [AdminGuard])


//taken from angular.io
//Only call RouterModule.forRoot in the root AppRoutingModule (or the AppModule if
//that's where you register top level application routes). In any other module, you
//must call the RouterModule.forChild method to register additional routes.

@NgModule({
    imports: [
        RouterModule.forChild(adminRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AdminRoutingModule {
}
