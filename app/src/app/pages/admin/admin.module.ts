import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {IonicModule} from '@ionic/angular'
import {RouterModule} from '@angular/router'

import {TranslateModule} from "@ngx-translate/core"
import {AdminHomePage} from "./home/admin-home"
import {AdminRoutingModule} from "./admin-routing.module"
import {PhotoReviewPage} from "./photoReview/photo-review"
import {ReportedUserPage} from "./reportedUser/reported-user"
import {ReportedUsersPage} from "./reportedUsers/reported-users"
import {AdminUserSearchPage} from "./userSearch/user-search"
import {AdminUserPage} from "./user/admin-user"

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        TranslateModule,
        AdminRoutingModule
    ],
    declarations: [
        AdminHomePage,
        PhotoReviewPage,
        ReportedUserPage,
        ReportedUsersPage,
        AdminUserSearchPage,
        AdminUserPage
    ]
})
export class AdminModule {
}
