import { Component } from '@angular/core';
import {Log} from "ng-log"
import {UIUtil} from "../../UIUtil"
import {IUser} from "../../../../../../server/src/data-model"
import {AdminService} from "../../../services/admin.service"
import {AlertController} from "@ionic/angular"

@Component({
    selector: 'admin-user',
    templateUrl: 'admin-user.html'
})
export class AdminUserPage {

    log: Log = new Log(AdminUserPage.name)
    user: IUser

    constructor(private adminService: AdminService,
                private uiUtil: UIUtil,
                private alertCtrl: AlertController) {
        // this.user = navParams.get('user')
    }


    public deleteUser(user: IUser) {
        // this.alertCtrl.create({
        //     message: 'Confirm delete',
        //     buttons: [
        //         {
        //             text: 'Cancel', handler: () => {
        //         }
        //         }, {
        //             text: 'Ok',
        //             handler: async () => {
        //                 this.log.log('deleteUser')
        //                 try {
        //                     await this.uiUtil.loading(this.adminService.deleteUser(user.id))
        //                 } catch (e) {
        //                     this.log.error('Error deleting user', user)
        //                     this.uiUtil.toast('Error deleting user')
        //                 }
        //             }
        //         }]
        // }).present().catch(this.log.error)
    }

    public banUser(user: IUser) {
        // this.alertCtrl.create({
        //     message: 'Confirm ban',
        //     buttons: [
        //         {
        //             text: 'Cancel', handler: () => {}
        //         }, {
        //             text: 'Ok',
        //             handler: async () => {
        //                 this.log.log('banUser')
        //                 try {
        //                     await this.uiUtil.loading(this.adminService.banUser(user.id))
        //                 } catch (e) {
        //                     this.log.error('Error banning user', user)
        //                     this.uiUtil.toast('Error banning user')
        //                 }
        //             }
        //         }]
        // }).present().catch(this.log.error)
    }
}
