import {Log} from "ng-log"
import {UIUtil} from "../../UIUtil"
import {Component} from "@angular/core"
import * as _ from 'lodash'
import {ReportedUserPage} from "../reportedUser/reported-user"
import {Router} from "@angular/router"
import {AdminService} from "../../../services/admin.service"
import {IReport, ReportedUserDetails} from "../../../../../../server/src/data-model"

@Component({
    selector: 'reported-users',
    templateUrl: 'reported-users.html'
})
export class ReportedUsersPage {

    log: Log = new Log(ReportedUsersPage.name)
    reports: IReport[]

    constructor(private adminService: AdminService,
                private router: Router,
                private uiUtil: UIUtil,) {}

    async ionViewWillEnter() {
        // this.reports = await this.uiUtil.loading(this.adminService.getReportedUsers())
        // console.log('reports', this.reports)
    }

    async viewDetails(report: IReport) {
        // const reportDetails: ReportedUserDetails = await this.uiUtil.loading(this.adminService.getReportedUserDetails(report))
        //
        // console.log('recentMessages', reportDetails.recentMessages)
        //
        // // Extract the messages that were sent to the user who raised the report
        // reportDetails.recentMessagesToReporter =
        //     _.filter(reportDetails.recentMessages, (msg: IChatMessage) => {
        //         if(msg.match && msg.match.userIds)
        //             return msg.match.userIds.indexOf(report.reportedBy) > -1
        //         return true
        //     })
        // // Just show the 6 most recent messages
        // if (reportDetails.recentMessagesToReporter &&
        //     reportDetails.recentMessagesToReporter.length > 6)
        //     reportDetails.recentMessagesToReporter.slice(0, 5)
        //
        // console.log('recentMessagesToReporter', reportDetails.recentMessagesToReporter)
        //
        // this.nav.push(ReportedUserPage, {report: report, reportDetails: reportDetails})
    }

}
