import {ComponentFixture, async} from '@angular/core/testing'
import {AdminService} from "../../../services/admin/AdminService"
import {ReportedUsersPage} from "./reported-users"
import {TestUtils} from "../../../test"

let fixture: ComponentFixture<ReportedUsersPage> = null
let instance: any = null

let adminServiceStub = {
    isLoggedIn: true,
}
const providers = [{provide: AdminService, useValue: adminServiceStub}]

describe('ReportedUsersPage', () => {

    beforeEach(async(() => TestUtils.beforeEachCompiler([ReportedUsersPage], providers).then(compiled => {
        fixture = compiled.fixture
        instance = compiled.instance
    })))

    it('should create the ReportedUsersPage', async(() => {
        expect(instance).toBeTruthy()
    }))
})