import {Log} from "ng-log"
import {UIUtil} from "../../UIUtil"
import {Component} from "@angular/core"
import {AdminUserPage} from "../user/admin-user"
import {AdminService} from "../../../services/admin.service"
import {Router} from "@angular/router"
import {IUser} from "../../../../../../server/src/data-model"

@Component({
    selector: 'photo-review',
    templateUrl: 'photo-review.html'
})
export class PhotoReviewPage {

    log: Log = new Log(PhotoReviewPage.name)
    searchType: 'id' | 'name' | 'email' = 'id'
    users: IUser[] | null = null
    // selected user for viewing details
    user: IUser

    constructor(private adminService: AdminService,
                private router: Router,
                private uiUtil: UIUtil,) {}

    async search(filter: string) {
        this.log.log('search', this.searchType, filter)
        try {
            switch(this.searchType) {
                case 'id':
                    const user: IUser | null = await this.uiUtil.loading(this.adminService.loadUser(filter))
                    this.users = user ? [user] : []
                    break
                case 'name':
                    this.users = await this.uiUtil.loading(this.adminService.searchUsersByName(filter))
                    break
                case 'email':
                    this.users = await this.uiUtil.loading(this.adminService.searchUsersByEmail(filter))
                    break
            }
            this.log.info('Found ' + (this.users === null ? 'null' : this.users.length))
        } catch (e) {
            this.log.error('Error searching by email', e)
            this.uiUtil.toast('Error loading users')
        }
    }

    view(user: IUser) {
        // this.nav.push(AdminUserPage, {user: user})
    }

    onCancel() {
        this.users = null
    }

}

