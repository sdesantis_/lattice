import {ComponentFixture, async} from '@angular/core/testing'
import {PhotoReviewPage} from "./photo-review"
import {AdminService} from "../../../services/admin/AdminService"
import {TestUtils} from "../../../test"

let fixture: ComponentFixture<PhotoReviewPage> = null
let instance: any = null

let adminServiceStub = {
    isLoggedIn: true,
}
const providers = [{provide: AdminService, useValue: adminServiceStub}]

describe('PhotoReviewPage', () => {

    beforeEach(async(() => TestUtils.beforeEachCompiler([PhotoReviewPage], providers).then(compiled => {
        fixture = compiled.fixture
        instance = compiled.instance
    })))

    it('should create the PhotoReviewPage', async(() => {
        expect(instance).toBeTruthy()
    }))
})