import { Component } from '@angular/core';
import {Log} from "ng-log"
import {AdminUserSearchPage} from "../userSearch/user-search"
import {ReportedUsersPage} from "../reportedUsers/reported-users"
import {PhotoReviewPage} from "../photoReview/photo-review"
import {Router} from "@angular/router"

@Component({
    selector: 'admin-home',
    templateUrl: 'admin-home.html'
})
export class AdminHomePage {

    log: Log = new Log(AdminHomePage.name)
    actions: any[]

    constructor(private router: Router) {
        this.actions = [
            {id: 'userSearch', url: 'admin-user-search', description: 'User search', page: AdminUserSearchPage},
            {id: 'reportedUsers', url: 'reported-users', description: 'Reported Users', page: ReportedUsersPage},
            {id: 'photoReview', description: 'Photo Review', page: PhotoReviewPage},
            // Share is in the html template
        ]
    }


    navTo(action: any) {
        this.router.navigate(action.url).catch(this.log.error)
    }
}
