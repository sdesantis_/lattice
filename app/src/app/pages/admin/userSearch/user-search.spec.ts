import {ComponentFixture, async} from '@angular/core/testing'
import {AdminUserSearchPage} from "./user-search"
import {AdminService} from "../../../services/admin/AdminService"
import {TestUtils} from "../../../test"

let fixture: ComponentFixture<AdminUserSearchPage> = null
let instance: any = null

let adminServiceStub = {
    isLoggedIn: true,
}
const providers = [{provide: AdminService, useValue: adminServiceStub}]

describe('AdminUserSearchPage', () => {

    beforeEach(async(() => TestUtils.beforeEachCompiler([AdminUserSearchPage], providers).then(compiled => {
        fixture = compiled.fixture
        instance = compiled.instance
    })))

    it('should create the AdminUserSearchPage', async(() => {
        expect(instance).toBeTruthy()
    }))
})