import { FormControl, FormGroup } from '@angular/forms';

export class CustomValidators{
    static validateEmail(c: FormControl) {
        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        return EMAIL_REGEXP.test(c.value) ? null : {emailValid: true};
    }

    static passwordsMatch(c: FormGroup){
        let passwords: any = {
            password: c.controls['password'],
            passwordConfirm: c.controls['passwordConfirm']
        }

        // reset all password errors
        for (let pwdField in passwords){
            passwords[pwdField].setErrors(null);
        }

        // check if currentPassword is empty
        if (passwords.password.value == ''){
            passwords.password.setErrors({required: true});
            return null;
        }

        if (passwords.password.value != passwords.passwordConfirm.value)
            passwords.passwordConfirm.setErrors({passwordMismatch: true})

        return null
    }
}