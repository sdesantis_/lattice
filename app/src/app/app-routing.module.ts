import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'
import {AuthGuard} from "./services/auth-guard.service"
import {AdminGuard} from "./services/admin-guard.service"

export class RoutePaths {
    static readonly LOGIN: string = 'login'
}

// using loadChildren with a string reference (instead of component with an type reference) is to enable lazy loading
const routes: Routes = [
    // {
    //     path: '',
    //     redirectTo: 'home',
    //     pathMatch: 'full'
    // },
    { path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
    {
        path: RoutePaths.LOGIN, loadChildren: './pages/login/login.module#LoginPageModule'
    },
    // {
    //     path: 'home',
    //     canActivate: [AuthGuard],
    //     loadChildren: './pages/home/home.module#HomePageModule'
    // },
    // {
    //     path: 'admin',
    //     canActivate: [AuthGuard],
    //     loadChildren: './pages/admin/admin.module#AdminModule'
    // },
    // {
    //     path: 'settings',
    //     canActivate: [AuthGuard],
    //     loadChildren: './pages/settings/settings.module#SettingsPageModule'
    // },
    // {
    //     path: 'list',
    //     loadChildren: './pages/list/list.module#ListPageModule'
    // },
]


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
