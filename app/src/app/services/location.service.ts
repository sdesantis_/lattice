import {Injectable} from '@angular/core'
import {Geolocation, Geoposition} from '@ionic-native/geolocation/ngx'
import {Log} from "ng-log"
import {AlertController, Platform} from "@ionic/angular"
import {ILocation} from "../../../../server/src/data-model"
import {LocationAccuracy} from "@ionic-native/location-accuracy/ngx"
import {Diagnostic} from "@ionic-native/diagnostic/ngx"

declare var cordova: any

@Injectable()
export class LocationService {

    log: Log = new Log(LocationService.name)

    constructor(private platform: Platform, private alertCtrl: AlertController, private geolocation: Geolocation,
                private locationAccuracy: LocationAccuracy, private diagnostic: Diagnostic) {}

    /**
     * Checks if the location services (Google Play Location services on Android) are enabled, and if not enabled then
     * asks the user to enable it.
     * @return Promise<void>
     */
    async requestLocationServices(): Promise<void> {
        this.log.log('requestLocationServices()')

        if (!this.platform.is('cordova'))
            return

        const canRequest: boolean = await this.locationAccuracy.canRequest()
        this.log.log('locationAccuracy.canRequest() ' + canRequest)

        try {
            // the accuracy option will be ignored by iOS
            await this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
            this.log.log('locationAccuracy request successful')

        } catch (error) {
            this.log.error('Error requesting location permissions', error)
            if (error) {
                // Android only
                if (error.code !== this.locationAccuracy.ERROR_USER_DISAGREED) {

                    const overlay = await this.alertCtrl.create({
                        // title: 'Use this lightsaber?',
                        message: "Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?",
                        buttons: [
                            {
                                text: 'Cancel', handler: () => this.log.log('Disagree clicked')
                            }, {
                                text: 'Ok',
                                handler: () => {
                                    this.log.log('diagnostic.switchToLocationSettings()')
                                    this.diagnostic.switchToLocationSettings()
                                }
                            }]
                    })
                    overlay.present().catch(this.log.warn)
                }
            }
        }
    }

    /**
     * Get the current location of the user. This check the app has permission to get the users location
     * @return Promise<ILocation>
     */
    async getCurrentPosition(): Promise<ILocation> {
        // see http://stackoverflow.com/questions/3397585/navigator-geolocation-getcurrentposition-sometimes-works-sometimes-doesnt
        this.log.log('getCurrentPosition()')
        var timeout = 10000

        // If on Android then use the plugin which uses the Fused Location Provider
        if (this.platform.is('android') && typeof cordova !== 'undefined' && cordova.plugins.locationServices) {
            this.log.debug('Using Android Fused Location Provider locationServices.geolocation.getCurrentPosition()')

            return new Promise<ILocation>((success, error) => {
                cordova.plugins.locationServices.geolocation.getCurrentPosition(
                    (position: Geoposition) => {
                        this.log.log('locationServices.geolocation.getCurrentPosition() = ', position)
                        if (!position || !position.coords) {
                            error('GEO_ERROR')
                        } else {
                            var geoPoint: ILocation = {
                                latitude: position.coords.latitude,
                                longitude: position.coords.longitude
                            }
                            success(geoPoint)
                        }
                    },
                    (error: any) => {
                        error('GEO_ERROR')
                        this.log.warn('locationServices.geolocation.getCurrentPosition error ', error)
                    }, {
                        timeout: timeout,
                        // priority: 102 // PRIORITY_BALANCED_POWER_ACCURACY
                        // priority: 104 // PRIORITY_LOW_POWER
                        priority: 100 // PRIORITY_HIGH_ACCURACY
                    })
            })
        }

        this.log.log('Geolocation.getCurrentPosition()')

        try {
            const options: PositionOptions = {maximumAge: 3600000, timeout: timeout, enableHighAccuracy: true}
            const position: Geoposition = await this.geolocation.getCurrentPosition(options)
            return {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            }
        } catch (e) {
            this.log.warn('Geolocation.getCurrentPosition error', e)
            throw 'GEO_ERROR'
        }
    }

}
