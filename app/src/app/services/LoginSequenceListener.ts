import {Injectable} from '@angular/core'
import {Log} from "ng-log"
import {LoginSequence} from "./LoginSequence"
import {UIUtil} from "../pages/UIUtil"
import {UserService} from "./user.service"
import {FacebookService} from "./facebook.service"
import {Router} from "@angular/router"
import {RoutePaths} from "../app-routing.module"
import {MenuController} from "@ionic/angular"
import {MainMenu} from "../pages/main-menu"

/**
 * This service listens to events emitted from LoginSequence.$next and then performs the logic to determine
 * the next page in the login sequence
 */
@Injectable()
export class LoginSequenceListener {

    log: Log = new Log(LoginSequenceListener.name)

    constructor(private router: Router,
                private userService: UserService,
                loginSequence: LoginSequence,
                private mainMenu: MainMenu,
                private facebookService: FacebookService,
                private uiUtil: UIUtil) {
        loginSequence.$next.subscribe(
            x => this.next(x),
            e => console.log('loginSequence onError: %s', e),
            () => console.log('loginSequence onCompleted'))
    }

    /**
     * Go to the next page in the login sequence
     * @returns {Promise<any>}
     */
    private async next(pushNotification: any): Promise<any> {
        this.log.log('next()')
        try {
            await this.doLoginSequence(pushNotification)
        } catch (e) {
            this.uiUtil.toast('Unable to login')
            this.log.error('Error in LoginSequenceListener.next()', e.message ? e.message : e)
            try {
                await this.userService.logout()
            } catch (e2) {
                this.log.warn('Error logging out', e2)
            }
            this.navigateTo(RoutePaths.LOGIN).catch(this.log.warn)
        }
    }

    async doLoginSequence(pushNotification: any): Promise<any> {
        const authUser: firebase.User | null = this.userService.currentAuthUser()

        if (authUser === null)
            return this.navigateTo(RoutePaths.LOGIN)

        const user = await this.userService.ensureCurrentUser()

        this.userService.init()

        if (user.status && user.status == 'banned') {
            this.uiUtil.toastTranslate('BANNED')
            return this.navigateTo(RoutePaths.LOGIN)
        }

        this.userService.registerPush()

        if (this.userService.isFacebookConnected())
            this.facebookService.authenticate()

        // If users have authenticated with Facebook then don't require email verification
        // Note: there are three states to emailVerified in Parse. undefined means it is not required, eg user signed up before email verification was enabled
        // if (!this.userService.isFacebookConnected() && user.emailVerified === false)
        //     return this.navigateTo(EmailVerificationPage)

        // Sign the user up to an email list for automated onboarding and marketing emails.
        // if(!user.subscribed)
        //     this.userService.subscribeToEmailList()

        // if(!await this.userService.isTermsOfUseAgreed())
        //     return this.navigateTo(TermsOfUsePage)

        // if (!this.userService.isValid())
        //     return this.navigateTo(ProfileSetupPage)

        // if (!user.location)
        //     return this.navigateTo(LocationSetupPage)

        // put welcome/intro slides here if youo like

        // Initialise any other main app services here

        // If we've opened from a push notification, then see if we want to navigate
        // to a particular screen for that notification
        if (pushNotification) {

        }
        // this.mainMenu.showMenu = true // If you wanted to enable a side menu once logged in, then uncomment this line
        return this.navigateTo('tabs/home')
    }

    navigateTo(pageUrl: string): Promise<any> {
        this.log.log('Navigating to ' + pageUrl)
        return this.router.navigateByUrl(pageUrl)
    }
}
