import {environment} from "../environment"
import {HttpClient} from "@angular/common/http"
import {UserService} from "./user.service"
import {HttpParams} from "@angular/common/http/src/params"
import {Log} from "ng-log"

export type ParamsType = HttpParams | {[param: string]: string | string[]; }
/**
 * Base class for calling cloud functions
 */
export class BaseFunctionsApiService {

    protected userService: UserService
    baselog = new Log(BaseFunctionsApiService.name)

    constructor(userService: UserService, protected http: HttpClient) {
        this.userService = userService
    }

    options(params?: ParamsType) {
        const headers: any = {headers: {authorization: 'Bearer ' + this.userService.idToken}}
        if(params)
            headers.params = params
        return headers
    }

    private path(path: string) {
        if(environment.environment === 'dev')
            return 'http://localhost:8080/' + path
        // /api/ comes from the name of the exported Firebase http function
        return `https://us-central1-${environment.gcpProjectId}.cloudfunctions.net/api/${path}`
    }

    protected get<T>(path: string, params?: ParamsType): T {
        this.baselog.debug('GET', path, params)
        return <T><any>this.http.get(this.path(path), this.options(params)).toPromise()
    }

    protected post<T>(path: string, params: ParamsType): T {
        this.baselog.debug('POST', path, params)
        return <T><any>this.http.post(this.path(path), params, this.options()).toPromise()
    }
}