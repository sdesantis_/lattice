import {Injectable} from '@angular/core'
import {Observable, Subject} from "rxjs/index"


/**
 * This service is called to go to the next step in the login sequence.
 *
 * It is implemented with the $next observable and a separate LoginSequenceListener class to avoid a circular
 * dependency between the pages and the LoginSequenceListener
 */
@Injectable()
export class LoginSequence {

    private _next = new Subject()
    $next: Observable<any> = this._next.asObservable()

    constructor() {}

    /**
     * Go to the next page in the login sequence.
     * If the app was opened from a push notification, then pass
     * the payload so the app can perform custom navigation if required
     */
    next(openPushNotification?: any): void {
        this._next.next(openPushNotification)
    }
}
