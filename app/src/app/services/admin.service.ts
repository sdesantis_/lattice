import {Injectable} from '@angular/core'
import {IReport, IUser, ReportedUserDetails} from "../../../../server/src/data-model"
import {Log} from "ng-log"
import {BaseFunctionsApiService} from "./base-functions-api"
import {HttpClient} from "@angular/common/http"
import {UserService} from "./user.service"


@Injectable()
export class AdminService extends BaseFunctionsApiService {

    log: Log = new Log(AdminService.name)

    constructor(userService: UserService, protected http: HttpClient) {
        super(userService, http)
    }

    getReportedUsers(): Promise<IReport[]> {
        return this.get('/admin/reportedUsers')
    }

    getReportedUserDetails(report: IReport): Promise<ReportedUserDetails> {
        return this.get('/admin/reportedUser', {id: report.id})
    }

    deletePhoto(reportId: string, photoUrl: string): Promise<void> {
        return this.post('/admin/deletePhoto', {reportId, photoUrl})
    }

    banUser(userId: string): Promise<void> {
        return this.post('/admin/banUser', {userId})
    }

    closeReport(reportId: string, action: string): Promise<void> {
        return this.post('/admin/closeReport', {reportId, action})
    }

    searchUsersByEmail(email: string): Promise<IUser[]> {
        return this.get('/admin/searchUsers', {email})
    }

    searchUsersByName(name: string): Promise<IUser[]> {
        return this.get('/admin/searchUsers', {name})
    }

    loadUser(userId: string): Promise<IUser | null> {
        return this.get('/admin/user', {userId})
    }

    deleteUser(userId: string): Promise<void> {
        return this.post('/admin/deleteUser', {userId})
    }

    getProfilesWithPhotosToReview(): Promise<IUser[]> {
        return this.get('/admin/photosToReview')
    }

    reviewPhoto(profileId: string, fileUrl: string, approved: boolean) {
        this.log.log('reviewPhoto', profileId, fileUrl, approved)
        if (!profileId) throw 'profileId is required'
        if (!fileUrl) throw 'fileUrl is required'
        return this.post('/admin/reviewPhoto', {profileId, fileUrl, approved: approved ? 'true' : 'false'})
    }
}
