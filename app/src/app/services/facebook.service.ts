import {Platform} from "@ionic/angular"
import {Log} from "ng-log"
import {Facebook, FacebookLoginResponse} from "@ionic-native/facebook/ngx"
import {Injectable} from "@angular/core"
import {environment} from "../environment"

// Native plugin global var
declare var facebookConnectPlugin: any
// variable for the JavaScript SDK
declare var FB: any

declare var window: any

/**
 * The response object returned by the [getAuthResponse](../facebook-service/#getAuthResponse) method, and is also used in [LoginResponse](../login-response/) and [LoginStatus](../login-status/).
 */
export interface AuthResponse {
    /** User access token */
    accessToken: string
    /** Access token lifetime in seconds */
    expiresIn: number
    signedRequest: string
    /** The Facebook user ID */
    userID: string
    /** The granted scopes. This field is only available if you set `return_scopes` to true when calling login method. */
    grantedScopes?: string
}

/**
 * The object returned by the [getLoginStatus](../facebook-service/#getLoginStatus) method.
 */
export interface LoginStatus {
    status: string
    authResponse: AuthResponse
}


/**
 * A basic Facebook service that uses the Native plugin if available, otherwise the Javascript SDK
 *
 * A more complete example is at https://github.com/jeduan/cordova-plugin-facebook4/blob/master/www/facebook-browser.js
 */
@Injectable()
export class FacebookService {

    static VERSION: String = 'v2.7'

    log: Log = new Log(FacebookService.name)
    native: boolean // If running on a Cordova platform and using the native plugin (required)
    appId: string // The Facebook app id
    errMsg = 'Facebook app Id has not been configured'

    constructor(private facebook: Facebook, platform: Platform) {
        this.native = platform.is('cordova')
        this.appId = environment.facebookAppId

        if (!this.appId)
            this.log.warn(this.errMsg)

        if (!this.native)
            this.loadJsSdk()
    }


    async authenticate(): Promise<AuthResponse> {

        let statusResult = await this.getLoginStatus()
        this.log.log('getLoginStatus:', statusResult)

        if (statusResult.status === 'connected')
            return statusResult.authResponse

        // If the user isn't connected then login
        let loginResult = await this.login(["public_profile", "email", "user_birthday", "user_photos", "user_friends", "user_likes"])
        this.log.log('login:', loginResult)

        if (loginResult.status !== 'connected')
            throw loginResult

        return loginResult.authResponse
    }


    async getLoginStatus(): Promise<LoginStatus> {
        this.log.log('getLoginStatus()')
        this.validate()

        if (this.native)
            return this.facebook.getLoginStatus()

        await this.waitForJsSdk()

        return new Promise<LoginStatus>((resolve, reject) => {
            try {
                FB.getLoginStatus((response: LoginStatus) => {
                    if (!response) {
                        reject()
                    } else {
                        resolve(response)
                    }
                })
            } catch (e) {
                reject(e)
            }
        })
    }


    async login(permissions: string[]): Promise<any> {
        this.log.log('login()')
        this.validate()

        if (this.native)
            return this.facebook.login(permissions)

        // JS SDK takes an object here but the native SDKs use array.
        const options: any = {}
        if (permissions && permissions.length > 0) {
            const index = permissions.indexOf('rerequest')
            if (index > -1) {
                permissions.splice(index, 1)
                options.auth_type = 'rerequest'
            }
            options.scope = permissions.join(',')
        }

        return new Promise((success, reject) => {
            this.log.log('FB.login', permissions)
            FB.login(function (response: FacebookLoginResponse) {
                response.authResponse ? success(response) : reject(response.status)
            }, options)
        })
    }

    async api(graphPath: string): Promise<any> {
        this.log.log(`api(${graphPath})`)
        this.validate()

        if (this.native)
            return this.facebook.api(graphPath, [])

        return new Promise((success, reject) => {
            // JS API does not take additional permissions
            FB.api(graphPath, function (response: any) {
                response.error ? reject(response.error) : success(response)
            })
        })
    }


    // ------------------------------------
    //  Util methods
    // ------------------------------------


    loadJsSdk() {
        if (window.location.protocol === "file:") {
            this.log.warn("Facebook JS SDK is not supported when using file:// protocol")
            return
        }

        this.log.log('Loading Facebook JS SDK')
        window.fbAsyncInit = () => {
            this.log.log('fbAsyncInit()')
            FB.init({
                appId: this.appId,
                xfbml: true,
                version: FacebookService.VERSION
            })
        }

        ;(function (d, s, id) {
            let js: HTMLScriptElement, fjs = d.getElementsByTagName(s)[0]
            if (d.getElementById(id)) {
                return
            }
            js = <HTMLScriptElement>d.createElement(s)
            js.id = id
            js.src = "//connect.facebook.net/en_US/sdk.js"
            if(fjs.parentNode)
                fjs.parentNode.insertBefore(js, fjs)
        }(document, 'script', 'facebook-jssdk'))
    }

    /**
     * Waits at most 5 seconds for the Facebook JavaScript SDK to load
     * @returns {Promise<void>}
     */
    async waitForJsSdk(): Promise<void> {

        if (!this.native) {
            const maxAttempts = 100
            let attempt = 0
            while (typeof FB === 'undefined' && ++attempt <= maxAttempts) {
                await new Promise(resolve => setTimeout(resolve, 50))
            }
            if (typeof FB === 'undefined') {
                throw 'Could not load Facebook SDK'
                // this.loadJsSdk() Should we trying loading it again?
            }
        }
    }


    /** Check the Facebook app has been configured, and if in Cordova that the native plugin has been installed */
    private validate() {
        if (!this.appId || this.appId == 'undefined') {
            window.alert(this.errMsg)
            throw this.errMsg
        }
        if (this.native && typeof facebookConnectPlugin === 'undefined') {
            window.alert("Facebook plugin is not installed")
            throw 'Facebook plugin is not installed'
        }
    }

}