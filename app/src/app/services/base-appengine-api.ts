import {environment} from "../environment"
import {HttpClient} from "@angular/common/http"
import {UserService} from "./user.service"

export class BaseAppEngineApiService {

    protected userService: UserService

    constructor(userService: UserService, protected http: HttpClient) {
        this.userService = userService
    }

    options(params?: any) {
        const headers: any = {headers: {authorization: 'Bearer ' + this.userService.idToken}}
        if (params)
            headers.params = params
        return headers
    }

    private path(path: string) {
        return environment.serverUrl + '/' + path
    }

    protected get<T>(path: string): T {
        return <T><any>this.http.get(this.path(path), this.options()).toPromise()
    }

    protected post<T>(path: string, params: any): T {
        return <T><any>this.http.post(this.path(path), params, this.options()).toPromise()
    }
}