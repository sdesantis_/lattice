import {Injectable, OnInit} from '@angular/core'
import {IUser} from "../../../../server/src/data-model"
import {Storage} from "@ionic/storage"
import {Log} from "ng-log"
import {AuthResponse, FacebookService} from "./facebook.service"
import {UIUtil} from "../pages/UIUtil"
import {AngularFireAuth} from "@angular/fire/auth"
import {environment} from "../../app/environment"
import {HttpClient} from "@angular/common/http"
import {Observable, Subject, Subscription} from "rxjs/index"
import * as firebase from 'firebase/app'
import {BaseFunctionsApiService} from "./base-functions-api"
import {Platform} from "@ionic/angular"
import {AngularFireMessaging} from "@angular/fire/messaging"
import {AngularFirestore, AngularFirestoreDocument} from "@angular/fire/firestore"


export interface IUserService {
    $loggedOut: Observable<void>

    login(email: string, password: string): Promise<IUser>

    facebookLogin(authResponse: AuthResponse): Promise<IUser>

    register(email: string, password: string): Promise<IUser>

    resetPassword(email: string): Promise<IUser>

    deleteAccount(): Promise<void>

    logout(): Promise<void>

    isLoggedIn(): boolean

    ensureCurrentUser(): Promise<IUser>

    currentUser(): IUser

    currentAuthUser(): firebase.User | null

    isFacebookConnected(): boolean

    isTermsOfUseAgreed(): Promise<boolean>

    isEmailVerified(): Promise<boolean>

    testPushNotification(): Promise<void>

    sendContactMessage(message: string): Promise<void>
}


@Injectable()
export class UserService extends BaseFunctionsApiService implements IUserService {

    private log = new Log(UserService.name)

    /** Observable for when the user logged out */
    private _loggedOut = new Subject<void>()
    $loggedOut: Observable<void> = this._loggedOut.asObservable()

    private _twilioAccessToken = new Subject<string>()
    $twilioAccessToken: Observable<string> = this._twilioAccessToken.asObservable()

    /** Firebase authentication token */
    public idToken: string | null

    facebookAccessToken: string

    private pushNotification: Subject<any> = new Subject<any>()
    $pushNotification: Observable<any> = this.pushNotification.asObservable()

    private pushNotificationOpen: Subject<any> = new Subject<any>()
    $pushNotificationOpen: Observable<any> = this.pushNotificationOpen.asObservable()

    private userDoc: AngularFirestoreDocument<IUser>
    private $user: Observable<IUser>

    private _currentUser: IUser

    constructor(private uiUtil: UIUtil,
                private afAuth: AngularFireAuth,
                private afs: AngularFirestore,
                private afMessaging: AngularFireMessaging,
                private facebookService: FacebookService,
                private platform: Platform,
                http: HttpClient,
                private storage: Storage) {
        super(null, http)
        afAuth.idToken.subscribe(token => this.idToken = token)

        afAuth.authState.subscribe(async user => {
            this.log.log('authState updated', user)
            if(user) {
                this.userDoc = afs.doc<IUser>('users/' + afAuth.auth.currentUser.uid)
                const userDocSnapshot = await this.userDoc.get().toPromise()
                this._currentUser = <IUser>userDocSnapshot.data()
                this.log.log('user doc:', this._currentUser)
                this.$user = this.userDoc.valueChanges()
                // this._currentUserSub = this.$user.subscribe(user => {this._currentUser = user; this.log.log('User doc updated', user)}, e => this.log.warn('Error getting user doc', e))
            } else {
                this.userDoc = null
                this.$user = null
            }
        })

        this.$pushNotification.subscribe(payload => {
            if (payload.type === 'test')
                uiUtil.toast('Test push received')
            if (payload.type === 'banned')
                this.logout()
        })
    }

    /** We should probably split authentication from the general user service so we don't need to do this */
    public init() {
        this.userService = this
    }

    refreshUser() {
    }

    /**
     * Registers for the FCM Push notifications
     */
    registerPush() {
        const FirebasePlugin: any = (window as any).FirebasePlugin
        if (FirebasePlugin) {
            FirebasePlugin.getToken(async (fcmToken: string) => {
                    this.log.log('FCM token', fcmToken)
                    if (this.currentUser().fcmToken !== fcmToken)
                        this.updateUser({fcmToken}).catch(e => this.log.error('Error saving FCM token', e))
                }, (error: any) => this.log.error('Error getting FCM token', error)
            )

            FirebasePlugin.onTokenRefresh((fcmToken: string) => {
                this.log.log('FCM token', fcmToken)
                this.updateUser({fcmToken}).catch(e => this.log.error('Error saving FCM token', e))
            }, (error: any) => {
                this.log.error(error)
            })

            FirebasePlugin.onNotificationOpen((notification: any) => {
                this.log.log('onNotificationOpen', notification)
                this.pushNotificationOpen.next(notification)
            }, (error: any) => this.log.log(error))
        }

        // Web/PWA push setup
        if (!this.platform.is('cordova')) {
            this.afMessaging.requestToken
                .subscribe(
                    async (fcmToken) => {
                        console.log('Push permission granted! Save to the server!', fcmToken)
                        if (this.currentUser().fcmToken !== fcmToken)
                            this.updateUser({fcmToken}).catch(e => this.log.error('Error saving FCM token', e))
                    },
                    (error) => {
                        console.error(error)
                    },
                )

            // const messaging = firebase.messaging()
            // messaging.usePublicVapidKey(environment.firebaseWebPushKey)
            //
            // messaging.requestPermission().then(() => {
            //     this.log.log('Web Push Notification permission granted.')
            //
            //     // Get Instance ID token. Initially this makes a network call, once retrieved
            //     // subsequent calls to getToken will return from cache.
            //     messaging.getToken().then((fcmToken: string) => {
            //         if (fcmToken) {
            //             this.log.log('Received FCM web token', fcmToken)
            //             this.updateUser({fcmToken}).catch(e => this.log.error('Error saving FCM token', e))
            //         } else {
            //             // Show permission request.
            //             this.log.log('No Instance ID token available. Request permission to generate one.')
            //             // Show permission UI.
            //             this.registerPush()
            //         }
            //     }).catch((err: any) => this.log.log('An error occurred while retrieving token', err))
            //
            // }).catch(err => this.log.error('Unable to get permission to notify.', err))
            //
            // // Handle incoming messages. Called when:
            // // - a message is received while the app has focus
            // // - the user clicks on an app notification created by a service worker
            // //   `messaging.setBackgroundMessageHandler` handler.
            // messaging.onMessage((payload: any) => {
            //     this.log.log('Message received. ', payload)
            //     this.pushNotificationOpen.next(payload)
            // })
        }
    }

    // TODO push notification handler when a new server version is deployed
    async upgradeRequired(): Promise<boolean> {
        const serverVersion: number = (<any>await this.get('apiVersion')).version
        const appVersion: number = environment.apiVersion
        return appVersion < serverVersion
    }


    register(email: string, password: string): Promise<any> {
        return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    }

    login(email: string, password: string): Promise<any> {
        this.log.log('login')
        return this.afAuth.auth.signInWithEmailAndPassword(email, password)
    }

    facebookLogin(authResponse: AuthResponse): Promise<any> {
        this.log.log('facebookLogin', authResponse)
        return this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
    }


    resetPassword(email: string): Promise<IUser> {
        throw 'not implemented'
    }

    async deleteAccount(): Promise<void> {
        this.log.log('deleteAccount')
        await this.afAuth.auth.currentUser.delete()
        this.logout()
    }

    async logout(): Promise<void> {
        this.log.log('logout')
        try {
            this._loggedOut.next()
        } catch (e) {
            this.log.error('Error in logged out subscribers', e)
        }
        try {
            await this.afAuth.auth.signOut()
        } catch (e) {
            // TODO Will we get an error if we've deleted the account?
            this.log.warn('Error logging out from server', e)
        }
        this.storage.clear().catch(this.log.warn)
        localStorage.clear()
        return
    }

    // /**
    //  * Method to call when a user has a logged in token, to re-authenticate
    //  */
    // authCheck(): Promise<void> {
    //     this.server.authCheck
    // }

    currentAuthUser(): firebase.User | null {
        return this.afAuth.auth.currentUser
    }

    async sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async ensureCurrentUser(): Promise<IUser> {
        // Wait up to 10 seconds for the user doc to load
        for(let i=0;i<100;i++) {
            if(!this._currentUser)
                await this.sleep(100)
        }
        if(this._currentUser)
            return this._currentUser
        throw 'Could not load the user'
    }

    currentUser(): IUser {
        return this._currentUser
    }

    isLoggedIn(): boolean {
        return this.currentUser() ? true : false
    }

    isAdmin(): boolean {
        console.error('TODO implement isAdmin')
        return this.isLoggedIn()
    }

    isFacebookConnected(): boolean {
        console.warn('isFacebookConnected defaulting to false')
        return false
        // return this.afAuth.auth.currentUser.providerData.find({})
    }

    async isTermsOfUseAgreed(): Promise<boolean> {
        return await this.storage.get('TermsOfUseAgreed')
    }

    setTermsOfUseAgreed() {
        this.storage.set('TermsOfUseAgreed', true).catch(this.log.error)
    }

    /**
     * Check if the user has verified their email address
     * @returns {Promise<boolean>} if the email is verified
     */
    async isEmailVerified(): Promise<boolean> {
        return this.currentUser().emailVerified === true
    }

    /**
     * Subscribes the user to an email marketing list
     */
    subscribeToEmailList(): Promise<void> {
        return this.get('subscribeToEmailList')
    }

    async updateUser(update: Partial<IUser>): Promise<void> {
        await this.userDoc.update(update)
    }

    /**
     * Sends a push notification to the user
     * @returns {Promise<void>}
     */
    testPushNotification(): Promise<void> {
        return this.get('testPushNotification')
    }

    /**
     * Sends a message to the admin/staff
     * @param {string} message - the message from the user
     * @returns {Promise} a promise which resolves when the message has been sent
     */
    sendContactMessage(message: string): Promise<void> {
        return this.post('contactMessage', {message})
    }

    /**
     *
     * @param token
     * @param {number} amount in dollars (This will get converted to the smallest currency unit (e.g. cents) on the server. See https://stripe.com/docs/api/node#create_charge-amount)
     * @param {string} product
     * @returns {Promise<void>}
     */
    async stripePurchase(token: any, amount: string, product: string) {
        this.log.log('stripePurchase', token, amount, product)
        const result: any = await this.post('stripe', {token, amount, product})
        return result.id
    }

    async setPremium(premium: boolean, product: any): Promise<void> {
        // await this.server.setPremium(premium, product)
        // await this.refresh()
        throw new Error('not implemented')
    }

    async addCredits(credits: number, product: any): Promise<void> {
        // await this.server.addCredits(credits, product)
        // await this.refresh()
        throw new Error('not implemented')
    }

}
