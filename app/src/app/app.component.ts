import {Component} from '@angular/core'

import {Platform} from '@ionic/angular'
import {SplashScreen} from '@ionic-native/splash-screen/ngx'
import {StatusBar} from '@ionic-native/status-bar/ngx'
import {TranslateService} from "@ngx-translate/core"
import {LoginSequenceListener} from "./services/LoginSequenceListener"
import {LogSubmitter} from "./services/log-submitter.service"
import {FacebookService} from "./services/facebook.service"
import {LoginSequence} from "./services/LoginSequence"
import {MainMenu} from "./pages/main-menu"

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    public appPages = [
        {
            title: 'Home',
            url: '/home',
            icon: 'home'
        },
        {
            title: 'Settings',
            url: '/settings',
            icon: 'settings'
        },
        {
            title: 'List',
            url: '/list',
            icon: 'list'
        }
    ]

    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private loginSequence: LoginSequence,
                public mainMenu: MainMenu, // used in the template
                loginListener: LoginSequenceListener, // This is required, even though not directly used here, to ensure there is always an instance of it listening
                logSubmitter: LogSubmitter, // This is required, even though not directly used here, to ensure there is always an instance subscribed to Log.$logEntry
                facebookService: FacebookService, // Reference here to load the Facebook Javascript SDK now if we need it
                private translate: TranslateService,) {
        this.initializeApp()
    }

    initializeApp() {
        this.platform.ready().then(() => {

            if (this.platform.is('cordova')) {
                this.statusBar.styleDefault()
                this.splashScreen.hide()
            }

            // This language will be used as a fallback when a translation isn't found in the current language
            this.translate.setDefaultLang('en')
            // The lang to use, if the lang isn't available, it will use the current loader to get them
            this.translate.use('en')

            this.loginSequence.next()
        })
    }
}
