import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {RouterModule, RouteReuseStrategy, Routes} from '@angular/router'

import {IonicModule, IonicRouteStrategy, Platform} from '@ionic/angular'
import {SplashScreen} from '@ionic-native/splash-screen/ngx'
import {StatusBar} from '@ionic-native/status-bar/ngx'

import {AppComponent} from './app.component'
import {AppRoutingModule} from './app-routing.module'
import {ComponentsModule} from './components/components.module'
import {Camera} from "@ionic-native/camera/ngx"
import {Keyboard} from "@ionic-native/keyboard/ngx"
import {LocationAccuracy} from "@ionic-native/location-accuracy/ngx"
import {Diagnostic} from "@ionic-native/diagnostic/ngx"
import {AppVersion} from "@ionic-native/app-version/ngx"
import {Facebook} from "@ionic-native/facebook/ngx"
import {SocialSharing} from "@ionic-native/social-sharing/ngx"
import {Media} from "@ionic-native/media/ngx"
import {Base64ToGallery} from '@ionic-native/base64-to-gallery/ngx'
import {UserService} from "./services/user.service"
import {LoginSequence} from "./services/LoginSequence"
import {LoginSequenceListener} from "./services/LoginSequenceListener"
import {HttpClientModule} from "@angular/common/http"
import {UIUtil} from "./pages/UIUtil"
import {AngularFireAuth} from "@angular/fire/auth"
import {FacebookService} from "./services/facebook.service"
import {LogSubmitter} from "./services/log-submitter.service"
import {MainMenu} from "./pages/main-menu"
import {IonicStorageModule} from "@ionic/storage"

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot(),
        AppRoutingModule,
        ComponentsModule,
        HttpClientModule
    ],
    providers: [
        StatusBar, SplashScreen, Diagnostic, LocationAccuracy, Camera, Keyboard, Base64ToGallery,
        AppVersion, Facebook, SocialSharing, Media,
        Platform,
        MainMenu, UIUtil, AngularFireAuth,
        FacebookService,
        UserService, LogSubmitter,
        LoginSequence, LoginSequenceListener,

        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
