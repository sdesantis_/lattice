#!/usr/bin/env node

const fs = require('fs')
const args = require('yargs').argv
var shell = require('shelljs')
var config = require('../../server/src/config.js')

const envConfig = config.envConfig
const env = config.env

const scriptFilename = 'app/scripts/copy-environment.js'

let serverUrl = envConfig.serverUrl

// When we are pushing dev builds to a phone, we want to use the ip address of the development computer WiFi network interface, and not localhost
// so the phone can connect to the server, provided it is also connected to the local WiFi network
if (serverUrl && serverUrl.indexOf('%LOCAL_IP%') !== -1) {
    serverUrl = serverUrl.replace('%LOCAL_IP%', require('./local-ip.js').getLocalIp())
    console.log('Using serverUrl = ' + serverUrl)
}

// echo '{"commit":"$CI_COMMIT_SHA"}' > app/www/commit.json

const jsOutput = `
export const environment = {
  appId: "${config.appId}",  
  appName: "${config.appName}",
  apiVersion: ${config.apiVersion},

  environment: "${config.env}",
  serverUrl: "${serverUrl}",

  gcpProjectId: "${envConfig.gcpProjectId}",
  gcpProjectNumber: "${envConfig.gcpProjectNumber}",
  gcpApiKey: "${envConfig.gcpApiKey}",
  firebaseServerKey: "${envConfig.firebaseServerKey}",
  firebaseWebPushKey: "${envConfig.firebaseWebPushKey}",

  stripeKey: "` + (config.env === 'prod' ? config.stripe.prod : config.stripe.test) + `",

  playStoreUrl: "${config.playStoreUrl}",
  itunesUrl: "${config.itunesUrl}",

  facebookAppId: "${config.facebookAppId}",
  facebookAppName: "${config.facebookAppName}",
  linkedInId: "${config.linkedInId}",
  linkedInSecret: "${config.linkedInSecret}",

  adMob: {
    android: {
      banner: "${config.adMob.android.banner}",
      interstitial: "${config.adMob.android.interstitial}"
    },
    ios: {
      banner: "${config.adMob.ios.banner}",
      interstitial: "${config.adMob.ios.interstitial}"
    }
  }
}
`
fs.writeFileSync('./src/app/environment.ts', jsOutput)

// -------------------------------------------------------------------------------
//  Write the Firebase push service work with the appropriate configuration
// -------------------------------------------------------------------------------

const firebaseServiceWorkerJs = `
// This file is generated from the copy-environment.js script. Do not edit or commit to source control!
// See https://firebase.google.com/docs/cloud-messaging/js/receive
importScripts('https://www.gstatic.com/firebasejs/5.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.2/firebase-messaging.js');

firebase.initializeApp(
    {
        apiKey: "${envConfig.gcpApiKey}",
        authDomain: "${envConfig.gcpProjectId}.firebaseapp.com",
        databaseURL: "https://${envConfig.gcpProjectId}.firebaseio.com",
        projectId: '${envConfig.gcpProjectId}',
        storageBucket: "${envConfig.gcpProjectId}.appspot.com",
        messagingSenderId: "${envConfig.gcpProjectNumber}"
    });

var messaging = firebase.messaging();

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    var notificationTitle = 'Background Message Title';
    var notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});
`
fs.writeFileSync('./src/firebase-messaging-sw.js', firebaseServiceWorkerJs)


// -------------------------------------------------------------------------------------
// Copy the files required for Firebase native builds
// -------------------------------------------------------------------------------------
var googleServices = 'google-services-' + env + '.json'

if(!fs.existsSync(googleServices)) {
    console.error('The file ' + googleServices + ' does not exist for the environment ' + env + '. FCM not available for Android')
    shell.rm('google-services.json') // Delete what may have been copied for a different environment
} else {
    shell.cp(googleServices, 'google-services.json')
}

var googleServiceInfo = 'GoogleService-Info-' + env + '.plist'

if(!fs.existsSync(googleServiceInfo)) {
    console.error('The file ' + googleServiceInfo + ' does not exist for the environment ' + env + '. FCM not available iOS')
    shell.rm('GoogleService-Info.plist') // Delete what may have been copied for a different environment
} else {
    shell.cp(googleServiceInfo, 'GoogleService-Info.plist')
}