var shell = require('shelljs')
var fs = require('fs')
var config = require('../../server/config.json');

if(!fs.existsSync('../ionicpro')) {
    console.log('Creating ionicpro directory')
    fs.mkdirSync('../ionicpro')
}

const result = shell.cd('../ionicpro')

// Make sure we exit before deleting all the files if we didn't change into the directory
if(result.code !== 0)
    process.exit(1)

// shell.rm('-rf', '.')

shell.cp('../app/config.xml', '.')
shell.cp('../app/ionic.config.json', '.')
shell.cp('../app/package.json', '.')
shell.cp('../app/yarn.lock', '.')
shell.cp('-R', '../app/src', '.')
