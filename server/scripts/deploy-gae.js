#!/usr/bin/env node
// Script to deploy to Google App Engine
const shell = require('shelljs')
const fs = require('fs')

const config = require('../src/config.js')
const env = config.env
const envConfig = config.envConfig

console.log('envConfig for ', env , envConfig)

if(!envConfig.gcpProjectId) {
    console.log('gcpProjectId is not configured in config.json for the environment ' + env)
    process.exit(1)
}

// Copy the .yaml file
const envYaml = 'app-' + env + '.yaml'
if(!fs.existsSync(envYaml)) {
    console.log('The file ' + envYaml + ' does not exist to deploy to the environment ' + env)
    process.exit(1)
}
console.log('Copying ' + envYaml + ' to app.yaml')
shell.cp(envYaml, 'app.yaml')


// Build the web version of the app
console.log('Building web app')
shell.cd('../app')
run('npm', [ 'run', `build-${env}`], '../app') // TOOD check exit status
// Copy it to the server
console.log('Copying app to server')
shell.cd('../server')
shell.rm('-rf', 'static')
shell.mkdir('static')
shell.cp('-R', '../app/www/*', 'static')
shell.touch('static/cordova.js') // This avoids a 404 error for the web version

// Compile the TypeScript locally. Its failing if we try and let the App Engine deployment do it from npm scripts
console.log('Compiling server code')
shell.rm('-rf', 'build')
shell.mkdir('build')
run('tsc')

// Deploy!
console.log('Deploying to App Engine...')
run('gcloud', [ 'app', 'deploy', '--quiet', '--stop-previous-version', '--project', envConfig.gcpProjectId])

// Logs are at C:\Users\<username>\AppData\Roaming\gcloud\logs\

// cwd is the current working directory to execute the command from. Only relevant for linux/OSX which doesn't like shell.exec. It should match where the shell currently is from calls to shell.cd('<dir>')
function run(command, args, cwd) {
    if (process.platform === 'win32')
        if(shell.exec(command + ' ' + (args ? args.join(' ') : '')).code !== 0) {
            console.log('App Engine deploy failed')
            process.exit(1)
        }
    else {
        var options = { stdio: 'inherit', stdin: 'inherit' }
        if(cwd)
            options.cwd = cwd
        if(require('child_process').spawnSync( command, args, options ).status !== 0) {
            console.log('App Engine deploy failed')
            process.exit(1)
        }
    }
}

