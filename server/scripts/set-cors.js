const fs = require('fs')
const shell = require('shelljs')

// Get the --env arg
const env = require('commander')
    .option('-e, --env <environment>', 'The environment to deploy to')
    .parse(process.argv).env
if(!env) throw '--env is required'

const config = require('../config.json')
if(!config[env]) {
    console.log('Configuration for the ' + env + ' environment does not exist in config.json')
    process.exit(1)
}

const projectId = config[env].gcpProjectId

// If you really wanted to lock down the CORS access you could have instead
// "origin": ["https://${projectId}.appspot.com", "http://localhost:8100", https://[custom domain],
const json =
`[
    {
        "origin": ["*"],
        "responseHeader": ["Content-Type"],
        "method": ["GET", "HEAD", "DELETE"],
        "maxAgeSeconds": 3600
    }
]`

const corsJsonFile = 'cors.json'
let fd = fs.openSync(corsJsonFile, 'w');
fs.writeSync(fd, json)

if (process.platform === 'win32') {
    shell.exec(`gsutil cors set cors.json gs://${projectId}.appspot.com`)
} else {
    require("child_process")
        .spawnSync( "gsutil", [ "cors", "set", "cors.json", `gs://${projectId}.appspot.com`],
            { stdio: "inherit", stdin: "inherit" } )
}

try {
    fs.unlinkSync(corsJsonFile)
} catch(e) {
    console.error('Error deleting ' + corsJsonFile, e)
}
