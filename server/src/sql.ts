import {Sequelize} from 'sequelize-typescript'

const envConfig = require('./config.js').envConfig

let sequalize: Sequelize | null = null

if (envConfig.sql && envConfig.sql.database) {
    const options: any = {
        database: envConfig.sql.database,
        dialect: 'postgresql',
        username: envConfig.sql.username,
        password: envConfig.sql.password,
        host: envConfig.sql.host,
        modelPaths: [__dirname + '/models']
    }
    sequalize = new Sequelize(options)
}

export default sequalize