// Save your local vars in .env for testing. DO NOT VERSION CONTROL `.env`!.
import {success} from './koa-util'

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') require('dotenv').config()

import * as Koa from 'koa'
import * as bodyParser from 'koa-bodyparser'
import * as chalk from 'chalk'
const cors = require('kcors')
import sequelize from './sql'
import adminRoutes from './admin/admin.routes'
import userRoutes from './user/user.routes'
import {IRouterContext} from "koa-router"
import * as Router from "koa-router"


const config = require('./config.js')


// Initialise Firebase
const admin = require('firebase-admin')
// When running in App Engine (i.e. GCLOUD_PROJECT env variable is set) then use default credential, otherwise use the service account key file
const gcpProjectId = process.env.GCLOUD_PROJECT
const credential = gcpProjectId ?
    admin.credential.applicationDefault() :
    admin.credential.cert(require(`../service-account-key-${config.env}.json`))

admin.initializeApp({
    credential: credential,
    databaseURL: `https://${config.envConfig.gcpProjectId}.firebaseio.com`
})


// Initialise Koa

const port = process.env.PORT || 8080
const app = new Koa()

// trust App Engine proxy
app.proxy = true
app.use(cors())
// @ts-ignore
app.use(bodyParser())
app.use(require('koa-static')('static', {}))

app.use(adminRoutes.routes()).use(adminRoutes.allowedMethods())
app.use(userRoutes.routes()).use(userRoutes.allowedMethods())

const router: Router = new Router()
router.get('/ping', async (ctx: IRouterContext) => success(ctx, 'pong'))
app.use(router.routes()).use(router.allowedMethods())

// db/tables and web server initialization
const sqlInitPromise = sequelize ? <Promise<any>><any>sequelize.sync() : Promise.resolve()
sqlInitPromise.then(() => {
    // Start Koa
    app.listen(port, () => console.log(chalk.default.bgGreen.bold(`Listening on port ${port}`)))
})
export default app
