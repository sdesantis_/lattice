import * as firebaseAdmin from 'firebase-admin'

export function sendPush(fcmToken: string, data: any, notification: firebaseAdmin.messaging.Notification | null, tag?: string) {
    const message: firebaseAdmin.messaging.Message = {
        data: data,
        token: fcmToken
    }
    if (notification)
        message.notification = notification

    if (tag)
        message.android = {notification: {tag}}

    console.debug('Sending FCM', message)

    // Send a message to the device corresponding to the provided registration token.
    firebaseAdmin.messaging().send(message)
        .then((response: any) => {
            // Response is a message ID string.
            console.debug('Successfully sent FCM message')
        })
        .catch((error: any) => {
            console.log('Error sending FCM message', message, error)
        })
}


