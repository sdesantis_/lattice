// simple module so we only read it from the filesystem once
// and it could be updated to apply environment variables
// or more dynamic configuration

var configFile = process.env.CONFIG_FILE || '../config.json'

// If we have specified a CONFIG_JSON environment variable, then prefer that
var config = process.env.CONFIG_JSON
if (config) {
    console.log('Using config from process.env.CONFIG_JSON')
    config = JSON.parse(config)
} else {
    // Otherwise load from file
    console.log('Loading config from ' + configFile)
    config = require(configFile)
}
if (!config) throw new Error('Could not get config')

console.log('config:', config)

let env = null
for (let i = 0; i < process.argv.length; i++) {
    if (process.argv[i] === '--env') {
        env = process.argv[i + 1]
        console.log('Using --env arg', env)
        break
    }
}
if(!env) {
    console.log('Checking ENV environment var:', process.env.ENV)
    env = process.env.ENV
}


if (!env) throw new Error('Could not determine the environment')
if (!config.environments[env]) throw new Error('No config for environment ' + env)

console.log('env:' + env)
config.env = env
config.envConfig = config.environments[env]

module.exports = config
