const config = require('./config.js')

// When running in App Engine (i.e. GCLOUD_PROJECT env variable is set) then use default credential, otherwise use the service account key file
const gcpProjectId = process.env.GCLOUD_PROJECT
const credential = gcpProjectId ?
    admin.credential.applicationDefault() :
    admin.credential.cert(require(`../../service-account-key-${config.env}.json`))

const gcloudAuth = gcpProjectId ? null : { projectId: config.envConfig.gcpProjectId, keyFilename: `../../service-account-key-${config.env}.json`}
var gcs = require('@google-cloud/storage')(gcloudAuth)
var bucket = gcs.bucket(config.envConfig.gcpProjectId + '.appspot.com')

console.log('Storage bucket Initialized')

module.exports = bucket