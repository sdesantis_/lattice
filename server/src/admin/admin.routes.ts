import * as Router from 'koa-router'
import {error, required, success} from '../koa-util'
import {AdminService} from './admin-service'
import {isAdminMiddleware} from '../middleware/isAdmin'
import {IRouterContext} from "koa-router"

const adminService = new AdminService()
const router: Router = new Router()

router.get('/admin/grantAdmin', isAdminMiddleware, async (ctx: IRouterContext) => {
    const userId = required(ctx, 'userId')
    try {
        await adminService.grantAdmin(userId)
        return success(ctx, {})
    } catch (e) {
        return error(ctx, e)
    }
})

router.get('/admin/revokeAdmin', isAdminMiddleware, async (ctx: IRouterContext) => {
    const userId = required(ctx, 'userId')
    try {
        await adminService.revokeAdmin(userId)
        return success(ctx, {})
    } catch (e) {
        return error(ctx, e)
    }
})

export default router


