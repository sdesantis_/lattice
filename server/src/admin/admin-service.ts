import * as firebaseAdmin from 'firebase-admin'

export class AdminService {

    async grantAdmin(userId: string) {
        const user = await firebaseAdmin.auth().getUser(userId)
        const claims: any = user.customClaims
        claims['admin'] = 1
        await firebaseAdmin.auth().setCustomUserClaims(userId, claims)
    }

    async revokeAdmin(userId: string) {
        const user = await firebaseAdmin.auth().getUser(userId)
        const claims: any = user.customClaims
        delete claims['admin']
        await firebaseAdmin.auth().setCustomUserClaims(userId, claims)
    }

}
