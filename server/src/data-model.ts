
export interface ILocation {
    longitude: number
    latitude: number
}

/**
 * This interface mirrors the Parse.Object API to allow compatibility with using Parse or a custom backend
 * See http://parseplatform.org/Parse-SDK-JS/api/classes/Parse.Object.html
 */
export interface IBase {

    /** The primary key of the object */
    id: string

    /** The date the object was created */
    createdAt: Date

    /** The date the object was last updated */
    updatedAt: Date
}

/**
 * @typedef {Object} User
 */
export interface IUser extends IBase {
    /**
     * undefined if the email verification is not required,
     * false if verification is still required
     * true if verified
     */
    emailVerified: boolean

    /** The status of the user account, eg banned, deleting. undefined by default. */
    status: undefined | null | 'banned' | 'deleting'

    /** If the user is an administrator */
    admin: boolean

    /** If the user has been subscribed to an email list (MailChimp, SendGrid etc) */
    subscribed: boolean

    /** Firebase Cloud Messaging token */
    fcmToken?: string

    // Profile data

    /** The first name of the user */
    name: string

    /** The birthdate of the user */
    birthdate: Date

    /** The age of the user in years */
    age: number

    /** The gender of the user. 'M' or 'F' is supported */
    gender: string

    /** Some information the user has provided about themself */
    about: string

    /** The user profile photos. */
    photos: string[]

    fbLikes: string[]

    // A partial birthday, if the user only give us permission for a partial birthdate value
    fbBirthday?: string

    hometown?: string

    /**
     * The user profile photos that are awaiting review by admin/moderators. '
     * Must be set undefined if there are none to enable the Parse.Query.exists() call to work when querying for them.
     */
    photosInReview: string[]

    /** The geo location of the user */
    location: ILocation
}

/**
 * The searchable values of a user which are copied to the SQL database for more flexible querying
 */
export interface IUserSearch {
    id: string
    /** The users birthdate in epoc millisedonds */
    birthdate: number
    /** The gender of the user. 'M' or 'F' is supported */
    gender: string

    long: number
    lat: number

    lastActive: Date
}


export interface IReport extends IBase {
    /** The user who submitted the report */
    reportedBy: string
    /** The user who was reported */
    reportedUser: string
    /** (optional) A particular photo URL that was reported */
    photo: string
    /** The reason for reporting */
    reason: string
    /** What action was taken by the admin (e.g. delete photo, warn, none, ban user) */
    actionTaken: string
    /** The admin user which took the action */
    actionUser: IUser
}

/** The details retured to the admin page for viewing the report against a user */
export interface ReportedUserDetails {
    allReports: IReport[]
    // recentMessages: IChatMessage[]
    // transient field set by the app
    // recentMessagesToReporter?: IChatMessage[]
}


export interface IContactMessage extends IBase {
    message: string
}
