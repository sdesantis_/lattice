import {Context} from 'koa'
import {IRouterContext} from 'koa-router'

export function uid(ctx: IRouterContext) {
    return ctx.state.token.uid
}

export function required(ctx: IRouterContext, property: string): string {
    const value = ctx.request.body[property]
    if (!value)
        throw Error(property + ' is required')
    return value
}

export function reqNumber(ctx: IRouterContext, property: string): number {
    const value = ctx.request.body[property]
    if (!value)
        throw Error(property + ' in body is required')
    if (Number.isInteger(value))
        return Number.parseInt(value)
    return Number.parseFloat(value)
}

export function reqParam(ctx: Context, property: string): number {
    const value = ctx.request.query[property]
    if (!value)
        throw Error(property + ' query param is required')
    return value
}

export function reqParamNumber(ctx: Context, property: string): number {
    const value = ctx.request.query[property]
    if (!value)
        throw Error(property + ' query param is required')
    if (Number.isInteger(value))
        return Number.parseInt(value)
    return Number.parseFloat(value)
}

export function fail(ctx: IRouterContext, data: any) {
    ctx.body = {status: 'fail', data: data}
    throw new Error(data)
}

export function success(ctx: IRouterContext, data: any) {
    ctx.body = data
    return ctx
}

export function error(ctx: IRouterContext, data: any) {
    ctx.body = {status: 'error', data: data}
    throw new Error(data)
}