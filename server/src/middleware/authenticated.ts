import * as firebaseAdmin from 'firebase-admin'
import {Context} from 'koa'

export async function authenticatedMiddleware(ctx: Context, next: Function) {

    let authorizationHeader: string | string[] | undefined = ctx.req.headers['authorization']
    if (Array.isArray(authorizationHeader)) {
        if (authorizationHeader.length > 0)
            authorizationHeader = authorizationHeader[0]
    }

    if (authorizationHeader) {
        try {
            const token = (<string>authorizationHeader).split(' ')
            const decodedToken = await firebaseAdmin.auth().verifyIdToken(token[1])
            ctx.state.token = decodedToken
            await next()
        } catch (err) {
            console.log(err)
            ctx.status = 401
        }
    } else {
        console.debug('Authorization header not found')
        ctx.status = 401
    }
}