import * as firebaseAdmin from 'firebase-admin'
import DecodedIdToken = firebaseAdmin.auth.DecodedIdToken
import UserRecord = firebaseAdmin.auth.UserRecord
import {authenticatedMiddleware} from './authenticated'
import {Context} from 'koa'

// Firebase claims https://medium.com/@hiranya911/firebase-introducing-bulk-user-export-and-custom-claims-e0187344c8ee

export async function isAdminMiddleware(ctx: Context, next: Function) {
    await authenticatedMiddleware(ctx, next)

    const token: DecodedIdToken = ctx.state.token
    if (!token) {
        ctx.status = 500
        throw new Error('No token')
    } // This middleware should always be after the firebaseAuth middleware

    const user: UserRecord = await firebaseAdmin.auth().getUser(token.uid)
    if (!user) {
        ctx.status = 401
        throw new Error('User doesn\'t exist')
    }

    if (!user.customClaims || !(<any>user.customClaims).admin) {
        ctx.status = 401
        throw new Error('User ' + token.uid + ' is not an admin')
    }

    next()
}