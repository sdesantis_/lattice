import {IUser} from '../data-model'
import * as firebaseAdmin from 'firebase-admin'
import {Firestore} from '@google-cloud/firestore'

// const sequalize = require('../db')
// const User = sequalize.User

export class UserService {

    fs = firebaseAdmin.firestore()

    public constructor() {
    }


    async getUserById(uid: string): Promise<IUser> {
        const userData = (await this.userDocRef(uid).get()).data()
        // userData will be undefined if the document doesn't exist
        if (!userData) throw new Error(this.userDocRef(uid).path + ' doesn`t exist')
        return <IUser>userData
    }

    userDocRef(uid: string): FirebaseFirestore.DocumentReference {
        return this.fs.collection('users').doc(uid)
    }

    async updateUser(uid: string, updates: Partial<IUser>) {
        // Initialize document
        const userRef = this.userDocRef(uid)
        // var setCity = userRef.update(updates)
        //
        // await this.fs.runTransaction(async t => {
        //     const doc = await t.get(userRef)
        //     // Add one person to the city population
        //     var newPopulation = doc.data()!.population + 1
        //     await t.update(userRef, {population: newPopulation})
        // })
    }


    async addPhoto(base64: string, userId: string): Promise<void> {
    }

}

// export function getUserById(id: number): Promise<IUser> {
//     return User.findById(id)
// }
