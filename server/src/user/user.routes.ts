import {UserService} from './user-service'
import {sendPush} from '../firebase-push'
import {error, success, uid} from '../koa-util'
import {authenticatedMiddleware} from '../middleware/authenticated'
import * as Router from "koa-router"
import {IRouterContext} from "koa-router"

const router: Router = new Router()
const userService = new UserService()

router.get('/testPushNotification', authenticatedMiddleware, async (ctx: IRouterContext) => {
    try {
        const userId = uid(ctx)
        const user = await userService.getUserById(userId)
        const fcmToken: string | undefined = user.fcmToken
        if (fcmToken)
            sendPush(fcmToken, {}, {title: 'Test notification', body: 'Push is working!'})
        else
            console.log('No FCM token for ', userId)
        success(ctx, {data: {}})
    } catch (e) {
        return error(ctx, e)
    }
})

export default router


