const fs = require('fs')
const spawn = require('cross-spawn')

fs.copyFileSync('../../server/config.json', './config.json')
fs.copyFileSync('../../server/src/data-model.ts', './src/data-model.ts')

var tsc = spawn("npx", ["tsc"], {stdio: [process.stdin, process.stdout, process.stderr]})
