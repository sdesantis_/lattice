const fs = require('fs')
const spawn = require('cross-spawn')

function copyServiceAccountKey() {
    console.log('Copying service-account-key-dev.json')
    fs.copyFileSync('../../server/service-account-key-dev.json', './service-account-key-dev.json')
}

function copyConfig() {
    console.log('Copying config.json')
    fs.copyFileSync('../../server/config.json', './config.json')
}

function copyDataModel() {
    console.log('Copying data-model.ts')
    fs.copyFileSync('../../server/src/data-model.ts', './src/data-model.ts')
}

copyServiceAccountKey()
copyConfig()
copyDataModel()

fs.watch('../../server/config.json', {encoding: 'buffer'}, (eventType, filename) => { if (filename) copyConfig() })
fs.watch('../../server/src/data-model.ts', {encoding: 'buffer'}, (eventType, filename) => { if (filename) copyDataModel() })

var server = spawn("node lib/index.js", ["--env", "dev",], {stdio: 'inherit'})
var tsc = spawn(".\\node_modules\\.bin\\tsc", ["-w"], {stdio: [process.stdin, process.stdout, process.stderr]})

// tsc.on('error', function(err) {
//     console.error('tsc: ' + err);
// })
// tsc.on('uncaughtException', function (err) {
//     console.error('tsc: ' + err)
// })
// tsc.on('exit', (code, signal) => {
//     if (code) {
//         console.error('Child exited with code', code)
//     } else if (signal) {
//         console.error('Child was killed with signal', signal);
//     } else {
//         console.log('Child exited okay')
//     }
// })

// tsc.stdout.on('data', (data) => {
//     console.log('tsc: ', data)
// })
// tsc.stderr.on('data', (data) => {
//     console.error('tsc: ', data)
// })
// server.stdout.on('data', (data) => {
//     console.log('server: ', data)
// })

function on_exit() {
    console.log('Exiting...')
    tsc.kill("SIGINT")
    server.kill("SIGINT")
    process.exit(0)
}

process.on('SIGINT', on_exit)
process.on('exit', on_exit)

setInterval(() => {
}, 1000)