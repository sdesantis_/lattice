import * as Router from "koa-router"
import {error, required, success} from "../koa-util"
import {AdminService} from "./admin-service"
import {isAdminMiddleware} from "../middleware/isAdmin"

const adminService = new AdminService()

const router: Router = new Router()

router.get('/admin/grantAdmin', isAdminMiddleware, async (ctx) => {
    const userId = required(ctx, 'userId')
    try {
        await adminService.grantAdmin(userId)
        success(ctx, {})
    } catch (e) {
        error(ctx, e)
    }
})

router.get('/admin/revokeAdmin', isAdminMiddleware, async (ctx) => {
    const userId = required(ctx, 'userId')
    try {
        await adminService.revokeAdmin(userId)
        success(ctx, {})
    } catch (e) {
        error(ctx, e)
    }
})

router.get('/admin/listAdmins', isAdminMiddleware, async (ctx) => {
    try {
        await adminService.listAdmins()
        success(ctx, {})
    } catch (e) {
        error(ctx, e)
    }
})

export default router


