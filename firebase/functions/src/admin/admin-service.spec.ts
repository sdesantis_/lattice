import * as admin from "firebase-admin"
import 'jest'

describe('AdminService', () => {

    const adminUid = 'theadmin'

    beforeAll(async () => {

        admin.auth().importUsers([{
            uid: adminUid,
            displayName: 'John Doe',
            email: 'johndoe@gmail.com',
            photoURL: 'http://www.example.com/12345678/photo.png',
            emailVerified: true,
            phoneNumber: '+11234567890',
            // Set this user as admin.
            customClaims: {admin: true},
            providerData: []
        }]).then(function(results) {
            results.errors.forEach(function(indexedError) {
                console.log('Error importing user ' + indexedError.index)
            })
        }).catch(function(error) {
            console.log('Error importing users:', error);
        })

    })

    // beforeEach(async () => {
    // })
    //
    // it('should create the app', async () => {
    // })
    // test('adds 1 + 2 to equal 3', () => {
    //     expect(3).toBe(3);
    // });

})
