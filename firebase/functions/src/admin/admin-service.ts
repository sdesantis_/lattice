import * as admin from 'firebase-admin'
import {IUser} from "../data-model"
import DocumentReference = admin.firestore.DocumentReference
import DocumentSnapshot = admin.firestore.DocumentSnapshot
import QuerySnapshot = admin.firestore.QuerySnapshot

// import * as firestore from '@google-cloud/firebase'
/**
 *
 */
export class AdminService {

    private firestore: admin.firestore.Firestore = admin.firestore()


    adminDocRef(uid: string): DocumentReference {
        return this.firestore.collection('admins').doc(uid)
    }

    async grantAdmin(userId: string) {
        const user = await admin.auth().getUser(userId)
        const claims: any = user.customClaims
        claims['admin'] = 1
        await admin.auth().setCustomUserClaims(userId, claims)
        try {
            await this.adminDocRef(userId).set({})
        } catch (e) {
            console.error('Failed to create admin document entry for', userId)
            throw e
        }
    }

    async revokeAdmin(userId: string) {
        const user = await admin.auth().getUser(userId)
        const claims: any = user.customClaims
        delete claims['admin']
        await admin.auth().setCustomUserClaims(userId, claims)
        try {
            await this.adminDocRef(userId).delete()
        } catch (e) {
            console.error('Failed to delete admin document entry for', userId)
            throw e
        }
    }

    async listAdmins(): Promise<IUser[]> {
        const adminsSnapshot: QuerySnapshot = await this.firestore.collection('admins').get()
        const userIds = adminsSnapshot.docs.map(doc => doc.id)
        return this.getUsers(userIds)
    }

    async getUsers(userIds: string[]): Promise<IUser[]> {
        if (userIds.length === 0) return []
        const docsRefs: DocumentReference[] = userIds.map(id => this.firestore.doc(`/users/${id}`))
        const docs: DocumentSnapshot[] = await this.firestore.getAll(docsRefs[0], ...docsRefs.slice(1))
        return docs.map(doc => <IUser><any>doc.data)
    }

    banUser() {
        // TODO
    }

    deleteUser() {
        // TODO
    }
}
