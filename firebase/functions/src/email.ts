const MailchimpApi = require('mailchimp-api-v3')

import config from './config'

console.log('email config', config)
console.log('config.mailchimp', config.mailchimp)

const mailchimp: any = config.mailchimp.apiKey ? new MailchimpApi(config.mailchimp.apiKey) : null
if (!config.mailchimp.apiKey)
    console.info('Mailchimp is not configured')

export function subscribeToList(listId: string, email: string) {
    return mailchimp.post(`/lists/${listId}/members`, {
        email_address: email,
        status: 'subscribed'
    })
}