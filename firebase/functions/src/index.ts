import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
const cors = require('kcors')
const Router = require('koa-router')

import config from './config'

// Initialise Firebase
// When running in the Google Cloud (i.e. GCLOUD_PROJECT env variable is set) then use default credential, otherwise use the service account key file
const gcpProjectId = process.env.GCLOUD_PROJECT
const initOptions = gcpProjectId ?
    functions.config().firebase : {
        credential: admin.credential.cert(require(`../service-account-key-${config.env}.json`)),
        databaseURL: `https://${config.envConfig.gcpProjectId}.firebaseio.com`,
        storageBucket: config.envConfig.gcpProjectId + '.appspot.com',
        projectId: config.envConfig.gcpProjectId
    }
admin.initializeApp(initOptions)


const Koa = require('koa')
const app = new Koa()
app.use(cors()) // This should be the first middleware
const router = new Router()
router.get('/ping', async (ctx) => {
    ctx.body = 'pong'
    return ctx
})
app.use(router.routes()).use(router.allowedMethods())
import adminRouter from './admin/admin.routes'
app.use(adminRouter.routes()).use(adminRouter.allowedMethods())

// If we're running locally in development then start up a web server so we can call the functions via HTTP
if (!process.env.GCLOUD_PROJECT) {
    const port = config.port || 8080
    app.listen(port, () => console.debug(`Koa is running on port ${port}`))
}

// Export the Koa web app as a HTTP function
// The name of the exported const becomes part of the URL to call the function
// i.e. the api in https://us-central1-lattice-dev1.cloudfunctions.net/api/
export const api = functions.https.onRequest(app.callback())
// Export our Firebase Auth functions
export * from './auth'