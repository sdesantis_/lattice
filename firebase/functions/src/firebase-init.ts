// Initialise Firebase
// When running in the Google Cloud (i.e. GCLOUD_PROJECT env variable is set) then use default credential, otherwise use the service account key file
import * as functions from "firebase-functions"
import * as admin from 'firebase-admin'
import config from "./config"

const gcpProjectId = process.env.GCLOUD_PROJECT
const initOptions = gcpProjectId ?
    functions.config().firebase : {
        credential: admin.credential.cert(require(`../service-account-key-${config.env}.json`)),
        databaseURL: `https://${config.envConfig.gcpProjectId}.firebaseio.com`,
        storageBucket: config.envConfig.gcpProjectId + '.appspot.com',
        projectId: config.envConfig.gcpProjectId
    }
admin.initializeApp(initOptions)