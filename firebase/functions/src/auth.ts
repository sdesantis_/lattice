import * as admin from 'firebase-admin'
import {EventContext} from "firebase-functions/lib/cloud-functions"
import {UserRecord} from "firebase-functions/lib/providers/auth"
import {subscribeToList} from './email'
import * as functions from "firebase-functions"
import {IUser} from "./data-model"

const config = require('./config')

const db: admin.firestore.Firestore = admin.firestore()

export type UserData = Partial<IUser> & admin.firestore.DocumentData

type DocRef = admin.firestore.DocumentReference
type DocSnapshot = admin.firestore.DocumentSnapshot

export const authUserOnCreate = functions.auth.user().onCreate(async (user: UserRecord, context: EventContext) => {
    console.log('user.onCreate', user)

    const userId = user.uid
    const userRef: DocRef = db.collection('users').doc(userId)
    try {
        await userRef.set({
            uid: userId,
            email: user.email
        })
        console.debug('Init user ' + userId)
    } catch (e) {
        console.error('Error initialising Firestore for user', userId, e)
    }

    try {
        await userRef.set({
            uid: userId,
            email: user.email
        })
        console.debug('Init user ' + userId)
        await subscribeToEmailList(userRef)
    } catch (e) {
        console.error('Error subscribing to email list for user', userId, e)
    }
    /*
    db.update('INSERT INTO "User"(fid, name, email, emailVerified, state, "creationDate", "updatedOn") VALUES ($1, $2, $3, $4, $5, $6, $7);',
        [user.uid, user.displayName, user.email, user.emailVerified, user.disabled ? 'D' : 'A', Date.now(), Date.now()],
        (error, results) => {
            if (error)
                console.error('onCreate error', user, error)
            else
                console.debug('Created user', user.uid, user.displayName, user.email)
        })
    */
})


/**
 * Subscribes the user to a email list (e.g MailChimp) for followup emails
 */
async function subscribeToEmailList(userRef: DocRef) {
    const userDoc: DocSnapshot = await userRef.get()
    const user: UserData = userDoc.data()
    if (user.subscribed === true) return

    const email = user.email

    if (!email) {
        console.log('No email address found')
        return
    }

    console.log('Registering with email', email)
    const subscribedUpdate: Partial<IUser> = {subscribed: true}
    try {
        const result = await subscribeToList(config.mailchimp.listId, email)
        console.debug(result)
        // save a subscription id from the result?
        await userRef.update(subscribedUpdate)
    } catch (e) {
        if (e.title === 'Member Exists') {
            userRef.update(subscribedUpdate).catch(error => console.error('Error setting subscribed flag', e))
        } else {
            console.error(`Error registering email address ${email} in Mailchimp for user ${user.id}`, e)
        }
    }
}


export const authUserOnDelete = functions.auth.user().onDelete((user: UserRecord, context: EventContext) => {
    const users = db.collection("users")
    const userId = user.uid
    users.doc(userId).update({
        status: 'D'
    }).then(() => {
        console.log('Set ' + userId + ' deleted')
    })

    // db.update('Update "User" set (state, "deletedAt") VALUES ($1, $2) where fid=$3;',
    //     ['X', Date.now(), user.uid],
    //     (error, results) => {
    //         if (error)
    //             console.error('onDelete error', user, error)
    //         else
    //             console.debug('Deleted user', user.uid, user.email)
    //     })
})