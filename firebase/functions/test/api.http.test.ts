import 'jest'

const Supertest = require(`supertest`)
const supertest = Supertest(process.env.BASE_URL)
import * as admin from 'firebase-admin'
import * as init from './firebase-init'

const adminUid = 'testZZZ'


describe('AdminService', () => {

    const adminUid = 'theadmin'

    beforeAll(async () => {
        require('./firebase-init')
        return admin.auth().importUsers([{
            uid: adminUid,
            displayName: 'John Doe',
            email: 'johndoe@gmail.com',
            photoURL: 'http://www.example.com/12345678/photo.png',
            emailVerified: true,
            phoneNumber: '+11234567890',
            // Set this user as admin.
            customClaims: {admin: true},
            providerData: []
        }])
    })

    test(`Test ping route`, async () => {
        const response = await supertest.get(`/ping`)
        expect(response.statusCode.toBe(200))
        expect(response.text.toBe(`pong`))
    })

    test(`Accessing an admin route without authorization should fail`, async () => {
        const response = await supertest.get(`/admin/listAdmins`)
        expect(response.statusCode.toBe(401))
    })

})