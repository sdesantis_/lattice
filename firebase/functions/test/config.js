const config = require('../config.json')

// When running in the Google Cloud (i.e. GCLOUD_PROJECT env variable is set) then use environment matching the project Id
const gcpProjectId = process.env.GCLOUD_PROJECT
if(gcpProjectId) {
    for (const envName of Object.keys(config.environments)) {
        const envConfig = config.environments[envName]
        if (envConfig.gcpProjectId === gcpProjectId) {
            config.envConfig = envConfig
            config.env = envName
            console.log('Using environment configuration for ' + envConfig + ' ' + gcpProjectId)
            break
        }
    }
} else {
    let env = 'dev'
    for (let i = 0; i < process.argv.length; i++) {
        if (process.argv[i] === '--env') {
            env = process.argv[i + 1]
            console.log('Using --env arg', env)
            break
        }
    }
    config.envConfig = config.environments[env]
    if(!config.envConfig)
        throw new Error('Invalid env ' + env)
    config.env = env
}
console.log('envConfig', config.envConfig)

module.exports = config