#!/usr/bin/env node
const fs = require('fs')
const isWin = process.platform === "win32";
const config = require('../server/src/config.js')
const envConfig = config.envConfig

// -------------------------------------------------------------------------------------
//  Copy the required values from server/config.json to firebase/functions/config.json
// -------------------------------------------------------------------------------------

const output =
`{
  "env": "${config.env}"
}
`

/*
,
  "db": {
      "host": "${envConfig.db.host}",
      "database": "${envConfig.db.database}",
      "username": "${envConfig.db.username}",
      "password": "${envConfig.db.password}"
  }
 */

fs.copyFileSync('../server/src/data-model.ts', './functions/src/data-model.ts')
fs.copyFileSync('../server/config.json', './functions/config.json')
// fs.writeFileSync('./functions/src/config.json', output)

const options = { stdio: "inherit", stdin: "inherit", shell: true } // options.cwd = cwd

console.log('Compiling functions....')
let result = require("child_process").spawnSync( 'npm', ['run', isWin ? 'build-win' : 'build'], {...options, 'cwd': './functions'})
if(result.status !== 0)
    throw new Error('Failed to compile functions. ' + JSON.stringify(result))

console.log('Deploying functions....')
const deployArgs = ['deploy', '--only', 'functions', '-P', config.envConfig.gcpProjectId]
// If we have a Firebase token environment var set (e.g. in continous integration build) then use it
if(process.env.FIREBASE_TOKEN) {
    console.log('Using firebase token')
    deployArgs.push('--token')
    deployArgs.push(process.env.FIREBASE_TOKEN)
} else if(process.env.CI) {
    // If we are in the continuous integration environment and don't have the FIREBASE_TOKEN env var then we won't be able to deploy
    throw new Error('You must set the FIREBASE_TOKEN variable in the GitLab CI settings for your project')
}
result = require("child_process").spawnSync(isWin ? 'functions\\node_modules\\.bin\\firebase' : 'functions/node_modules/.bin/firebase', deployArgs, options )
if(result.status !== 0)
    throw new Error('Failed to deploy functions. ' + JSON.stringify(result))

console.log('Deployed')